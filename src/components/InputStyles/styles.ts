import styled from 'styled-components';
import { TextField, AppBar } from '@material-ui/core';
import {
  createStyles,
  makeStyles,
  Theme,
  withStyles,
} from '@material-ui/core/styles';

export const Input = withStyles(theme => ({
  root: {
    backgroundColor: '#FAFBFC',
    'label + &': {
      marginTop: theme.spacing(3),
    },
    background: 'white',
    width: '100%',
    [theme.breakpoints.down('sm')]: {
      margin: '10px',
    },
    [theme.breakpoints.up('md')]: {
      margin: '0px',
    },
    [theme.breakpoints.up('lg')]: {
      margin: theme.spacing(1),
    },
  },
}))(TextField);

export const MenuTab = withStyles(() => ({
  root: {
    width: '250px',
    border: '0',
    boxShadow: '0',
    margin: '10px',
  },
}))(AppBar);

export const Linha = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;

  @media (min-width: 481px) and (max-width: 767px) {
    flex-direction: column;
  }

  @media (min-width: 320px) and (max-width: 480px) {
    flex-direction: column;
  }

  @media (min-width: 768px) and (max-width: 1024px) {
    flex-direction: column;
  }
`;

export const Form = styled.div`
  background-color: #fff;
  box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
  border-radius: 8px;
  margin: 30px;
  padding: 20px;

  @media (min-width: 481px) and (max-width: 767px) {
    margin-left: 10px;
    margin-right: 10px;
  }

  @media (min-width: 320px) and (max-width: 480px) {
    margin-left: 10px;
    margin-right: 10px;
  }

  @media (min-width: 768px) and (max-width: 1024px) {
    margin-left: 10px;
    margin-right: 10px;
  }
`;

export const TitleForm = styled.div`
  margin: 10px;
  @media (min-width: 481px) and (max-width: 767px) {
    margin-left: 0px;
  }

  @media (min-width: 320px) and (max-width: 480px) {
    margin-left: 0px;
  }

  @media (min-width: 768px) and (max-width: 1024px) {
    margin-left: 0px;
  }
`;

export const Divider = styled.div`
  border: 0.1px solid rgba(0, 0, 0, 0.12);
  flex: none;
  order: 1;
  height: 0px;
  flex-grow: 0;
  margin: 20px 0px;
`;

interface TabsProps {
  active: boolean;
}

export const Tabs = styled.div`
  overflow: hidden;
  position: absolute;
  font-family: Open Sans;
  margin-top: 5px;

  @media (min-width: 481px) and (max-width: 767px) {
    margin-top: 75px;
  }

  @media (min-width: 320px) and (max-width: 480px) {
    margin-top: 75px;
  }

  @media (min-width: 768px) and (max-width: 1024px) {
    margin-top: 75px;
  }
`;

export const Tab = styled.button`
  border: none;
  outline: none;
  cursor: pointer;
  font-size: 1em;
  padding-top: 15px;
  margin-right: 30px;
  font-weight: bold;
  color: ${(props: TabsProps) => (props.active ? 'black' : '#1B458D')};
  border-bottom: ${(props: TabsProps) =>
    props.active ? '3px solid #1b458d' : ''};
  background-color: transparent;

  transition: background-color 0.5s ease-in-out;

  :hover {
    background-color: white;
    border-bottom-width: 3px;
    border-bottom-color: #1b458d;
  }
`;

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      width: '100%',
      backgroundColor: '#FAFBFC',
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  }),
);

export const useStylesInput = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      width: '100%',
      backgroundColor: '#FAFBFC',
    },
  }),
);

/* Header tabela cadastro turmas  */
export const RowHeader = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const RowTitle = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;

  h3 {
    margin-left: 10px;
    margin-top: 3px;
    font-family: Nunito;
    font-style: normal;
    font-weight: 600;
    font-size: 16px;
    color: rgba(0, 0, 0, 0.6);
  }
`;

/* Component Save  */
export const RowFooter = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;
`;

/* Opções da tabela */
export const ButtonOptions = styled.button`
  background-color: transparent;
  border: 0;
  margin-left: 10px;
`;

/* footer form */
export const Footer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;

  #btn-cancelar {
    font-family: 'Nunito';
    font-style: normal;
    font-weight: 600;
    font-size: 16px;
    line-height: 22px;
    border: 0;
    background-color: transparent;
    padding: 10px;
    color: rgba(0, 0, 0, 0.6);
    width: 126px;
    height: 46px;
  }

  #btn-salvar {
    font-family: 'Nunito';
    font-style: normal;
    font-weight: 600;
    font-size: 16px;
    line-height: 22px;
    border: 0;
    background: #1b458d;
    color: #fff;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    border-radius: 50px;
    padding: 10px;
    color: rgba(0, 0, 0, 0.6);
    width: 126px;
    height: 46px;

    p {
      color: #fff;
    }

    img {
      margin-right: 10px;
    }

    :hover {
      filter: brightness(0.9);
    }
  }
`;
