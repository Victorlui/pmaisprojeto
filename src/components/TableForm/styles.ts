import styled from 'styled-components';

export const Table = styled.table`
  width: 100%;
  caption-side: top;
  border-collapse: collapse;
  border-radius: 8px;
  text-align: left;
  border-left: 1px solid rgba(0, 0, 0, 0.12);
  border-right: 1px solid rgba(0, 0, 0, 0.12);
  border-bottom-left-radius: 8px;

  thead {
    background-color: #1b458d;
    padding: 10px;
    color: #fff;

    tr {
      background-color: #1b458d;
    }

    th:first-child {
      border-top-left-radius: 8px;
    }
    th:last-child {
      border-top-right-radius: 8px;
    }
    th {
      padding: 10px;
    }
  }

  tbody {
    padding: 10px;

    td {
      padding: 1em;
      border-bottom: 1px solid rgba(0, 0, 0, 0.12);
    }
  }
`;
