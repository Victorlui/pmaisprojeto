import { useState } from 'react';
import { Edit, DeleteGray } from '../../assets/icons';
import { Table } from './styles';

type Props = {
  head: any[];
  data: any;
  children: React.ReactNode;
};

const TableForm = ({ data, head, children }: Props) => {
  const [empty] = useState('');

  return (
    <Table>
      <thead>
        <tr>
          {head.map(d => (
            <>
              <th>{d.title}</th>
            </>
          ))}
          <th>{empty}</th>
        </tr>
      </thead>
      <tbody>{children}</tbody>
    </Table>
  );
};

export default TableForm;
