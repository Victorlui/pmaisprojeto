import styled from 'styled-components';

export const TopoForm = styled.div`
  background-color: #fff;
  padding: 32px;
  border-bottom: 1px solid #d3d3d3;
  width: 100%;
  height: 155px;
  @media (min-width: 481px) and (max-width: 767px) {
    height: auto;
  }

  @media (min-width: 320px) and (max-width: 480px) {
    height: auto;
  }

  @media (min-width: 768px) and (max-width: 1024px) {
    height: auto;
  }
`;

export const RowTab = styled.div`
  display: flex;
  flex-direction: row;
`;
