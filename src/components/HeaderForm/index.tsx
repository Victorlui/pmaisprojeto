import { Column, Title, Subtitle, Row } from 'pmaistemplates-v2';
import { Tabs, Tab } from '../index';
import { TopoForm, RowTab } from './styles';

interface HeaderFormProps {
  active: number;
  title: string;
  subTitle: string;
  tabs?: boolean;
  titleActive1?: string;
  titleActive2?: string;
  onTabChange?: any;
  buttonTop?: boolean;
  children?: JSX.Element;
}

const HeaderForm = ({
  active,
  title,
  subTitle,
  tabs = false,
  titleActive1,
  titleActive2,
  onTabChange,
  buttonTop,
  children,
}: HeaderFormProps) => {
  return (
    <TopoForm>
      <Row>
        <Column>
          <Title size="large">{title}</Title>
          {subTitle && <Subtitle color="gray">{subTitle}</Subtitle>}
          {tabs && (
            <RowTab>
              <Tabs>
                <Tab onClick={() => onTabChange(0)} active={active === 0}>
                  {titleActive1}
                </Tab>

                <Tab onClick={() => onTabChange(1)} active={active === 1}>
                  {titleActive2}
                </Tab>
              </Tabs>
            </RowTab>
          )}
        </Column>
        {buttonTop && <>{children}</>}
      </Row>
    </TopoForm>
  );
};

HeaderForm.defaultProps = {
  tabs: false,
  titleActive1: 'Tab 1',
  titleActive2: 'Tab 2',
  onTabChange: () => {
    console.log('tab');
  },
  buttonTop: false,
  children: <p>Component</p>,
};

export default HeaderForm;
