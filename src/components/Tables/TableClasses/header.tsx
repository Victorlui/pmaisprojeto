import { useStyles } from './styles';
import { ClassesListProps } from '../../../store/modules/schools/types';
import { Order } from './functions';

export interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
  onRequestSort: (
    event: React.MouseEvent<unknown>,
    property: keyof ClassesListProps,
  ) => void;
  order: Order;
  orderBy: string;
  headCells: HeadCell[];
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
  numSelected: number;
  rowCount: number;
}

export interface HeadCell {
  disablePadding: boolean;
  id: keyof ClassesListProps;
  label: string;
  numeric: boolean;
}

export const headCellsClasses: HeadCell[] = [
  {
    id: 'nameTurma',
    numeric: false,
    disablePadding: true,
    label: 'Nome da Turma',
  },
  {
    id: 'segmento',
    numeric: false,
    disablePadding: false,
    label: 'Segmento',
  },
  {
    id: 'serie',
    numeric: false,
    disablePadding: false,
    label: 'Série/Ano',
  },
  {
    id: 'curso',
    numeric: true,
    disablePadding: false,
    label: 'Tipo de curso',
  },
  {
    id: 'licencas',
    numeric: false,
    disablePadding: false,
    label: 'Licenças',
  },
  {
    id: 'licencas',
    numeric: false,
    disablePadding: false,
    label: '',
  },
];
