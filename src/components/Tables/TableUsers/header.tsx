import { useStyles } from './styles';
import { UsersListProps } from '../../../store/modules/schools/types';
import { Order } from './functions';

export interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
  onRequestSort: (
    event: React.MouseEvent<unknown>,
    property: keyof UsersListProps,
  ) => void;
  order: Order;
  orderBy: string;
  headCells: HeadCell[];
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
  numSelected: number;
  rowCount: number;
}

export interface HeadCell {
  disablePadding: boolean;
  id: keyof UsersListProps;
  label: string;
  numeric: boolean;
}

export const headCellsClasses: HeadCell[] = [
  {
    id: 'name',
    numeric: false,
    disablePadding: true,
    label: 'Nome',
  },
  {
    id: 'user',
    numeric: false,
    disablePadding: false,
    label: 'Usuário',
  },
  {
    id: 'perfil',
    numeric: false,
    disablePadding: false,
    label: 'Perfil',
  },
  {
    id: 'status',
    numeric: false,
    disablePadding: false,
    label: 'Status',
  },
  {
    id: 'status',
    numeric: false,
    disablePadding: false,
    label: '',
  },
];
