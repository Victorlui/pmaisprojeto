import { useStyles } from './styles';
import { ClassesProps } from '../../../store/modules/schools/types';
import { Order } from './functions';

export interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
  onRequestSort: (
    event: React.MouseEvent<unknown>,
    property: keyof ClassesProps,
  ) => void;
  order: Order;
  orderBy: string;
  headCells: HeadCell[];
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
  numSelected: number;
  rowCount: number;
}

export interface HeadCell {
  disablePadding: boolean;
  id: keyof ClassesProps;
  label: string;
  numeric: boolean;
}

export const headCellsClasses: HeadCell[] = [
  {
    id: 'name',
    numeric: false,
    disablePadding: true,
    label: 'Nome',
  },
  {
    id: 'user',
    numeric: false,
    disablePadding: false,
    label: 'Usuário',
  },
  {
    id: 'matricula',
    numeric: false,
    disablePadding: false,
    label: 'Matrícula',
  },
  {
    id: 'status',
    numeric: true,
    disablePadding: false,
    label: 'Status',
  },
  {
    id: 'status',
    numeric: false,
    disablePadding: false,
    label: '',
  },
];
