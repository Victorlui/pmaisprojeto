import { useStyles } from './styles';
import { ImportListProps } from '../../../store/modules/schools/types';
import { Order } from './functions';

export interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
  onRequestSort: (
    event: React.MouseEvent<unknown>,
    property: keyof ImportListProps,
  ) => void;
  order: Order;
  orderBy: string;
  headCells: HeadCell[];
}

export interface HeadCell {
  disablePadding: boolean;
  id: keyof ImportListProps;
  label: string;
  numeric: boolean;
}

export const headCellsClasses: HeadCell[] = [
  {
    id: 'name',
    numeric: false,
    disablePadding: true,
    label: 'Nome',
  },
  {
    id: 'data',
    numeric: false,
    disablePadding: false,
    label: 'Data',
  },
  {
    id: 'carregado',
    numeric: false,
    disablePadding: false,
    label: 'Carregado por',
  },
  {
    id: 'id',
    numeric: false,
    disablePadding: false,
    label: '',
  },
];
