import { makeStyles, withStyles, createStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import TablePagination from '@material-ui/core/TablePagination';
import styled from 'styled-components';

export const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },
  active: {
    color: 'white',
  },
}));

export const useToolbarStyles = makeStyles(theme => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: 'red',
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  title: {
    flex: '1 1 100%',
  },
}));

export const StyledTableCell = withStyles(() =>
  createStyles({
    head: {
      backgroundColor: '#1B458D',
      color: 'white',
      fontSize: 16,
    },
    body: {
      fontSize: 14,
      color: 'white',
      textAlign: 'center',
    },
    active: {
      color: 'red',
    },
  }),
)(TableCell);

export const StyledTableSort = withStyles(() =>
  createStyles({
    root: {
      color: 'white',
      '&:hover': {
        color: 'white',
      },
      '&$active': {
        color: 'white',
      },
    },
    active: {},
    icon: {
      color: 'inherit !important',
    },
  }),
)(TableSortLabel);

export const StyledTablePagination = withStyles(() =>
  createStyles({
    toolbar: {
      background: 'black',
    },
  }),
)(TablePagination);

export const useStylesCheck = makeStyles({
  root: {
    color: '#fff !important',
    '&$checked': {
      color: '#fff !important',
    },
  },
  checked: {},
});

export const StyledTablePagination2 = styled.div`
  background-color: red;
  display: flex;
  flex: 1;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
`;

export const RowPage = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding-left: 5px;
  padding-right: 5px;
  padding-top: 10px;
`;

export const RowItem = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;

  p {
    font-size: 14px;
    line-height: 19px;
    /* identical to box height */

    /* Greys/grey_dark */

    color: rgba(0, 0, 0, 0.87);
  }
`;

export const ButtonPage = styled.button`
  border: 0;
  background-color: #1b458d;
  padding: 5px;
  border-radius: 6px;
  color: white;
  margin-left: 5px;
  width: 40px;
  height: 40px;
`;

interface StatusBoxProps {
  status: any;
}

export const StatusBox = styled.div`
  background-color: ${(props: StatusBoxProps) =>
    props.status === '1' ? '#B1FFBA;' : '#FFEF9D'};
  padding: 8px;
  border-radius: 6px;
  text-align: center;
  width: 59px;
  height: 34px;
`;

/* Select items */
export const Select = styled.select`
  margin: 5px;
  border: 1px solid rgba(0, 0, 0, 0.12);
  padding: 8px;
  border-radius: 4px;

  option {
    color: black;
    background: white;
    font-weight: small;
    display: flex;
    white-space: pre;
    min-height: 20px;
    padding: 0px 2px 1px;
  }
`;

export const Option = styled.option`
  padding: 30px;
`;
