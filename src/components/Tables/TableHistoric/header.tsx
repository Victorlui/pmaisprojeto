import { useStyles } from './styles';
import { HistoricListProps } from '../../../store/modules/schools/types';
import { Order } from './functions';

export interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
  onRequestSort: (
    event: React.MouseEvent<unknown>,
    property: keyof HistoricListProps,
  ) => void;
  order: Order;
  orderBy: string;
  headCells: HeadCell[];
}

export interface HeadCell {
  disablePadding: boolean;
  id: keyof HistoricListProps;
  label: string;
  numeric: boolean;
}

export const headCellsClasses: HeadCell[] = [
  {
    id: 'unidade',
    numeric: false,
    disablePadding: true,
    label: 'Unidade',
  },
  {
    id: 'turma',
    numeric: false,
    disablePadding: false,
    label: 'Turma',
  },
  {
    id: 'curso',
    numeric: false,
    disablePadding: false,
    label: 'Curso',
  },
  {
    id: 'disciplina',
    numeric: false,
    disablePadding: false,
    label: 'Disciplina',
  },
  {
    id: 'status',
    numeric: false,
    disablePadding: false,
    label: 'Status',
  },
  {
    id: 'id',
    numeric: false,
    disablePadding: false,
    label: '',
  },
];
