import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paginations from '@material-ui/lab/Pagination';
import Checkbox from '@material-ui/core/Checkbox';

import Paper from '@material-ui/core/Paper';

import { Copy, Delete, Edit, More } from '../../../assets/icons';

import {
  useStyles,
  StyledTableCell,
  StyledTableSort,
  RowPage,
  StatusBox,
  RowItem,
  Select,
  useStylesCheck,
} from './styles';

import { getComparator, stableSort, Order } from './functions';
import { HistoricListProps } from '../../../store/modules/schools/types';
import { headCellsClasses, EnhancedTableProps } from './header';

interface TableProps {
  data: HistoricListProps[];
  limit: number;
  onChangeLimit(value: number): void;
  pagesNavigation?: number;
  totalItems?: number;
}

function EnhancedTableHead(props: EnhancedTableProps) {
  const { classes, order, orderBy, onRequestSort, headCells } = props;
  const createSortHandler = (property: keyof HistoricListProps) => (
    event: React.MouseEvent<unknown>,
  ) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => (
          <StyledTableCell
            key={headCell.id}
            align="left"
            padding="default"
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <StyledTableSort
              className={classes.active}
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </StyledTableSort>
          </StyledTableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

const TableHistoric = (props: TableProps) => {
  const classes = useStyles();
  const { data, onChangeLimit, limit, pagesNavigation, totalItems } = props;
  const [order, setOrder] = useState<Order>('asc');
  const [orderBy, setOrderBy] = useState<keyof HistoricListProps>('unidade');
  const [pages, setPages] = useState(0);
  const [selected, setSelected] = useState<string[]>([]);
  const history = useHistory();

  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof HistoricListProps,
  ) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleChangePage = (event: unknown, newPage: number) => {
    setPages(newPage);
  };

  function handleChangeRowsPerPage(
    event: React.ChangeEvent<HTMLSelectElement>,
  ) {
    onChangeLimit(parseInt(event.target.value, 10));
    setPages(0);
  }

  interface PaginationProps {
    page: number;
  }

  function Pagination({ page }: PaginationProps) {
    const numPage = Math.ceil(data.length / limit);

    return (
      <RowPage>
        <RowItem>
          <p>Mostrar</p>
          <Select
            value={limit}
            onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
              handleChangeRowsPerPage(event)
            }
          >
            <option>5</option>
            <option>10</option>
            <option>15</option>
            <option>20</option>
          </Select>
          <p>Entrada</p>
        </RowItem>
        <RowItem>
          <p>
            Mostrando {limit} de {totalItems} entradas
          </p>
        </RowItem>
        <RowItem>
          <Paginations
            count={pagesNavigation}
            shape="rounded"
            variant="outlined"
            onClick={(event: React.MouseEvent<HTMLElement>) => {
              console.log(page);
            }}
          />
        </RowItem>
      </RowPage>
    );
  }

  return (
    <div style={{ marginTop: 20 }}>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="customized table">
          <EnhancedTableHead
            classes={classes}
            order={order}
            orderBy={orderBy}
            onRequestSort={handleRequestSort}
            headCells={headCellsClasses}
          />
          {data.length === 0 ? (
            <TableRow>
              <TableCell />
              <TableCell />
              <TableCell style={{ padding: 20 }}>
                <p style={{ fontSize: 20 }}>Nenhum item encontrado</p>
              </TableCell>
              <TableCell />
              <TableCell />
            </TableRow>
          ) : (
            <TableBody>
              {stableSort(data, getComparator(order, orderBy))
                .slice(pages * limit, pages * limit + limit)
                .map((s, index) => {
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow
                      hover
                      onClick={event => console.log('teste')}
                      role="checkbox"
                      tabIndex={-1}
                      key={s.unidade}
                      style={{ cursor: 'pointer' }}
                    >
                      <TableCell
                        component="th"
                        id={labelId}
                        scope="row"
                        padding="default"
                        align="justify"
                      >
                        {s.unidade}
                      </TableCell>
                      <TableCell align="left">{s.turma}</TableCell>
                      <TableCell align="left">{s.curso}</TableCell>
                      <TableCell align="left">{s.disciplina}</TableCell>
                      <TableCell align="left" style={{ width: 30 }}>
                        <StatusBox status={s.status}>
                          {s.status === '0' ? 'Inativo' : 'Ativo'}
                        </StatusBox>
                      </TableCell>
                      <TableCell align="center">
                        <img src={More} alt="delete" />
                      </TableCell>
                    </TableRow>
                  );
                })}
            </TableBody>
          )}
        </Table>
      </TableContainer>
    </div>
  );
};

TableHistoric.defaultProps = {
  pagesNavigation: 1,
  totalItems: 1,
};

export default TableHistoric;
