import React from 'react';

import { Horizontal } from './styles';

interface RowProps {
  children: any;
  width?: string;
}

const Row: React.FC<RowProps> = ({ children, width = '100%' }: RowProps) => {
  return <Horizontal width={width}>{children}</Horizontal>;
};

Row.defaultProps = {
  width: '100%',
};

export default Row;
