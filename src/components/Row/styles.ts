import styled from 'styled-components';

interface RowProps {
  width: string;
}

export const Horizontal = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
  flex-direction: row;
  width: ${(p: RowProps) => p.width};
  padding: 10px;

  @media (min-width: 481px) and (max-width: 767px) {
    flex-direction: column;
    margin-top: 10px;
    width: 100%;
  }

  @media (min-width: 320px) and (max-width: 480px) {
    flex-direction: column;
    margin-top: 10px;
    width: 100%;
  }

  @media (min-width: 768px) and (max-width: 1024px) {
    flex-direction: column;
    margin-top: 10px;
    width: 100%;
  }
`;
