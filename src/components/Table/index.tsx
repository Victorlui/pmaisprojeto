import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paginations from '@material-ui/lab/Pagination';

import Paper from '@material-ui/core/Paper';

import { Folder } from '../../assets/icons';

import {
  useStyles,
  StyledTableCell,
  StyledTableSort,
  RowPage,
  StatusBox,
  RowItem,
  Select,
} from './styles';

import { getComparator, stableSort, Order } from './functions';
import { SchoolProps } from '../../store/modules/schools/types';
import { HeadCell, EnhancedTableProps } from './header';

interface TableProps {
  data: any[];
  limit: number;
  onChangeLimit(value: number): void;
  headCells: HeadCell[];
  pagesNavigation?: number;
  totalItems?: number;
}

function EnhancedTableHead(props: EnhancedTableProps) {
  const { classes, order, orderBy, onRequestSort, headCells } = props;
  const createSortHandler = (property: keyof SchoolProps) => (
    event: React.MouseEvent<unknown>,
  ) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => (
          <StyledTableCell
            key={headCell.id}
            align="left"
            padding="default"
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <StyledTableSort
              className={classes.active}
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </StyledTableSort>
          </StyledTableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

const TableComponent = (props: TableProps) => {
  const classes = useStyles();
  const {
    data,
    onChangeLimit,
    limit,
    headCells,
    pagesNavigation,
    totalItems,
  } = props;
  const [order, setOrder] = useState<Order>('asc');
  const [orderBy, setOrderBy] = useState<keyof SchoolProps>('nomeConta');
  const [pages, setPages] = useState(0);
  const history = useHistory();
  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: keyof SchoolProps,
  ) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleChangePage = (event: unknown, newPage: number) => {
    setPages(newPage);
  };

  /* const clickHandle = (event: any, page: number) => {
    console.log(page);
  }; */

  function handleChangeRowsPerPage(
    event: React.ChangeEvent<HTMLSelectElement>,
  ) {
    onChangeLimit(parseInt(event.target.value, 10));
    setPages(0);
  }

  interface PaginationProps {
    page: number;
  }

  function Pagination({ page }: PaginationProps) {
    const numPage = Math.ceil(data.length / limit);

    return (
      <RowPage>
        <RowItem>
          <p>Mostrar</p>
          <Select
            value={limit}
            onChange={(event: React.ChangeEvent<HTMLSelectElement>) =>
              handleChangeRowsPerPage(event)
            }
          >
            <option>5</option>
            <option>10</option>
            <option>15</option>
            <option>20</option>
          </Select>
          <p>Entrada</p>
        </RowItem>
        <RowItem>
          <p>
            Mostrando {limit} de {totalItems} entradas
          </p>
        </RowItem>
        <RowItem>
          <Paginations
            count={pagesNavigation}
            shape="rounded"
            variant="outlined"
            onClick={(event: React.MouseEvent<HTMLElement>) => {
              console.log(page);
            }}
          />
        </RowItem>
      </RowPage>
    );
  }

  return (
    <div style={{ padding: '20px' }}>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="customized table">
          <EnhancedTableHead
            classes={classes}
            order={order}
            orderBy={orderBy}
            onRequestSort={handleRequestSort}
            headCells={headCells}
          />
          {data.length === 0 ? (
            <TableRow>
              <TableCell />
              <TableCell />
              <TableCell style={{ padding: 20 }}>
                <p style={{ fontSize: 20 }}>Nenhum item encontrado</p>
              </TableCell>
              <TableCell />
              <TableCell />
            </TableRow>
          ) : (
            <TableBody>
              {stableSort(data, getComparator(order, orderBy))
                .slice(pages * limit, pages * limit + limit)
                .map((s, index) => {
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow
                      hover
                      onClick={event => history.push(`./createSchool`)}
                      role="checkbox"
                      tabIndex={-1}
                      key={s.nomeConta}
                      style={{ cursor: 'pointer' }}
                    >
                      <TableCell
                        component="th"
                        id={labelId}
                        scope="row"
                        padding="default"
                        align="justify"
                      >
                        <img
                          src={Folder}
                          alt="folder"
                          style={{ width: 15, marginRight: 5 }}
                        />
                        {s.nomeConta}
                      </TableCell>
                      <TableCell align="left">{s.id}</TableCell>
                      <TableCell align="left">{s.cidade}</TableCell>
                      <TableCell align="left">{s.segment}</TableCell>
                      <TableCell align="left">
                        <StatusBox status={s.ativo}>
                          {s.ativo === '0' ? 'Inativo' : 'Ativo'}
                        </StatusBox>
                      </TableCell>
                    </TableRow>
                  );
                })}
            </TableBody>
          )}
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component={Pagination}
        count={data.length}
        rowsPerPage={limit}
        page={pages}
        onChangePage={handleChangePage}
      />
    </div>
  );
};

TableComponent.defaultProps = {
  pagesNavigation: 1,
  totalItems: 1,
};

export default TableComponent;
