import { useStyles } from './styles';
import { SchoolProps, ClassesProps } from '../../store/modules/schools/types';
import { Order } from './functions';

export interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
  onRequestSort: (
    event: React.MouseEvent<unknown>,
    property: keyof SchoolProps,
  ) => void;
  order: Order;
  orderBy: string;
  headCells: any[];
}

export interface HeadCell {
  disablePadding: boolean;
  id: keyof any;
  label: string;
  numeric: boolean;
}

export const headCells: HeadCell[] = [
  {
    id: 'nomeConta',
    numeric: false,
    disablePadding: true,
    label: 'Nome',
  },
  {
    id: 'id',
    numeric: true,
    disablePadding: false,
    label: 'Usuário',
  },
  { id: 'cidade', numeric: false, disablePadding: false, label: 'Turmas' },
  {
    id: 'segment',
    numeric: false,
    disablePadding: false,
    label: 'Perfil',
  },
  {
    id: 'ativo',
    numeric: false,
    disablePadding: false,
    label: 'Status',
  },
];

export const headCellsSchools: HeadCell[] = [
  {
    id: 'nomeConta',
    numeric: false,
    disablePadding: true,
    label: 'Nome da Conta',
  },
  {
    id: 'id',
    numeric: true,
    disablePadding: false,
    label: 'ID Escola',
  },
  { id: 'cidade', numeric: false, disablePadding: false, label: 'Cidade' },
  {
    id: 'segment',
    numeric: false,
    disablePadding: false,
    label: 'Segmento ativo',
  },
  {
    id: 'ativo',
    numeric: false,
    disablePadding: false,
    label: 'Status',
  },
];

export const headCellsStudent: HeadCell[] = [
  {
    id: 'nomeConta',
    numeric: false,
    disablePadding: true,
    label: 'Unidade',
  },
  {
    id: 'id',
    numeric: true,
    disablePadding: false,
    label: 'Curso',
  },
  { id: 'cidade', numeric: false, disablePadding: false, label: 'Turma' },
  {
    id: 'id',
    numeric: false,
    disablePadding: false,
    label: '',
  },
  {
    id: 'ativo',
    numeric: false,
    disablePadding: false,
    label: 'Status',
  },
];

export const headCellsClasses: HeadCell[] = [
  {
    id: 'name',
    numeric: false,
    disablePadding: true,
    label: 'Nome',
  },
  {
    id: 'user',
    numeric: false,
    disablePadding: false,
    label: 'Usuário',
  },
  {
    id: 'matricula',
    numeric: false,
    disablePadding: false,
    label: 'Matrícula',
  },
  {
    id: 'status',
    numeric: true,
    disablePadding: false,
    label: 'Status',
  },
  {
    id: 'status',
    numeric: false,
    disablePadding: false,
    label: '',
  },
];
