import styled from 'styled-components';

export const Container = styled.header`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 5px 30px 5px;

  position: static;
  left: 0%;
  right: 0%;

  background: #ffffff;

  /* Inside Auto Layout */
  border-bottom: 1px solid #d3d3d3;
  flex: none;
  flex-grow: 0;
  margin: 0px 0px;
  box-shadow: 0px 2px 8px rgba(0, 0, 0, 0.15);
`;

interface Props {
  cor: string;
}

export const ContainerNav = styled.div`
  background-color: ${(props: Props) => props.cor};
  padding: 4px 24px 4px 24px;
  color: white;
  width: 100%;
`;

export const Profile = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const Icon = styled.img`
  margin: 10px;
`;

export const Avatar = styled.div`
  display: flex;
  flex-direction: row;
  cursor: pointer;
  align-items: center;
`;

export const TextEscola = styled.p`
  font-family: Nunito;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 16px;

  /* grey-medium */

  color: rgba(0, 0, 0, 0.6);

  @media (min-width: 481px) and (max-width: 767px) {
    display: none;
  }

  @media (min-width: 320px) and (max-width: 480px) {
    display: none;
  }
`;

export const TextAluno = styled.p`
  font-family: Nunito;
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 22px;
  /* identical to box height */

  /* grey-medium */

  color: rgba(0, 0, 0, 0.6);

  @media (min-width: 481px) and (max-width: 767px) {
    display: none;
  }

  @media (min-width: 320px) and (max-width: 480px) {
    display: none;
  }
`;

export const Divider = styled.hr`
  height: 30px;
  margin: 10px;
  border: 1px solid rgba(0, 0, 0, 0.12);
`;
