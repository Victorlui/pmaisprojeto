import React, { useState, useEffect } from 'react';
import { useRouteMatch } from 'react-router-dom';
import { useSelector } from 'react-redux';
import Logo from '../../assets/logo-header.png';
import AvatarIcon from '../../assets/avatar.png';
import Sino from '../../assets/sino.png';
import {
  Container,
  ContainerNav,
  Profile,
  Icon,
  Divider,
  Avatar,
  TextEscola,
  TextAluno,
} from './styles';

const Header: React.FC = () => {
  const { path, url } = useRouteMatch();

  const type = useSelector((state: any) => state.auth.type);

  const [corHeader, setCorHeader] = useState('#FAA41F');

  useEffect(() => {
    if (type === '2') {
      setCorHeader('#EE2252');
    }
  }, [type]);
  return (
    <>
      <ContainerNav cor={corHeader}>
        <p>Portal Edros</p>
      </ContainerNav>
      <Container>
        <img alt="logo" src={Logo} />

        <Profile>
          <Icon alt="aa" src={Sino} />
          <Divider />
          <Avatar>
            <img alt="aa" src={AvatarIcon} style={{ margin: 10 }} />

            <div>
              <TextEscola>Escola São Martins da Silva</TextEscola>
              <TextAluno>Bem vinda, Dir. Eliana!</TextAluno>
            </div>
          </Avatar>
        </Profile>
      </Container>
    </>
  );
};

export default Header;
