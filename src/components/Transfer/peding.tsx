import React, { useState } from 'react';
import { Checkbox, OutlinedInput, InputAdornment } from '@material-ui/core';
import { Search } from '../../assets/icons';
import { Container, Item, Content } from './styles';

interface ItemsProps {
  id: number;
  title: string;
  status: string;
}

interface PeddingProps {
  items: ItemsProps[];
  setItems: any;
  updateStatus: any;
}

const Pedding = ({ items, setItems, updateStatus }: PeddingProps) => {
  const [workDays, setWorkDays] = useState<Array<number>>([]);

  function handleCheckboxChange(
    event: React.ChangeEvent<HTMLInputElement>,
    id: number,
  ) {
    let a: number[] = [];
    a = [...workDays, id];

    if (workDays.includes(id)) {
      a = a.filter((ids: number) => ids !== id);
    }

    setWorkDays(a);
  }

  return (
    <Container>
      <OutlinedInput
        style={{ width: '100%', marginTop: '20px' }}
        placeholder="Busque pelo nº do livro"
        endAdornment={
          <InputAdornment position="end">
            <img alt="search" src={Search} />
          </InputAdornment>
        }
      />
      <Content>
        {items.map((item: ItemsProps) => (
          <Item key={item.id}>
            <Checkbox
              color="primary"
              value={item.id}
              onChange={(e: React.ChangeEvent<HTMLInputElement>): void => {
                handleCheckboxChange(e, item.id);
                updateStatus(item.id, 'Completed');
              }}
            />
            <div>
              <span>Nº do livro</span>
              <p>{item.title}</p>
            </div>
          </Item>
        ))}
      </Content>
    </Container>
  );
};

export default Pedding;
