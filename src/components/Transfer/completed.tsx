import React from 'react';
import { Select } from 'pmaistemplates-v2';
import { Delete } from '../../assets/icons';
import { Container, Content, Item, BtnDelete } from './styles';

interface ItemsProps {
  id: number;
  title: string;
  status: string;
}

interface PeddingProps {
  items: ItemsProps[];
  setItems: any;
  updateStatus: any;
}

const Completed = ({ items, setItems, updateStatus }: PeddingProps) => {
  return (
    <Container>
      <Content>
        {items.map((item: ItemsProps) => (
          <>
            {item.status === 'Completed' && (
              <Item key={item.id}>
                <BtnDelete
                  type="button"
                  onClick={() => {
                    updateStatus(item.id, 'Pending');
                  }}
                >
                  <img src={Delete} alt="delete" />
                </BtnDelete>
                <div>
                  <span>Nº do livro</span>
                  <p>{item.title}</p>
                </div>
              </Item>
            )}
          </>
        ))}
      </Content>
      <div style={{ margin: '20px' }} />
      <Select
        width="100%"
        name="expiracao"
        label="Expiração"
        options={items}
        onSelect={(event: React.ChangeEvent<HTMLSelectElement>) => {
          console.log('selecionado');
        }}
      />
    </Container>
  );
};

export default Completed;
