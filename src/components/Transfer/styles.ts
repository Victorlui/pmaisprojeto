import styled from 'styled-components';

export const Container = styled.div`
  background-color: #fff;
  padding-left: 10px;
  padding-right: 10px;
  background: #ffffff;
  /* Box-shadow-1 */

  box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
  border-radius: 8px;
  height: 596px;
  margin: 10px;
`;

export const Content = styled.div`
  /* for Firefox */
  overflow-y: scroll;
  margin-top: 20px;
  height: 480px;
`;

export const Item = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin: 10px;
  font-family: Nunito;
  font-style: normal;
  font-weight: normal;

  img {
    margin: 15px;
  }

  div {
    span {
      font-size: 14px;
      line-height: 19px;
      /* identical to box height */

      display: flex;
      align-items: center;

      /* Greys/grey_medium */

      color: rgba(0, 0, 0, 0.6);
    }
  }
`;

export const BtnDelete = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  border: 0px;
  padding: 0;
  margin: 0;
  background-color: transparent;
`;
