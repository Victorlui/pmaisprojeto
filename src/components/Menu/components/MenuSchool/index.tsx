import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { List, ListItemIcon, ListItemText, Collapse } from '@material-ui/core';

import { pageRequest } from '../../../../store/modules/page/actions';

import { IdIcon, Home, Group2, Download } from '../../../../assets/icons';

import { ListItemStyle, useStyles } from './styles';

const MenuSchool: React.FC = () => {
  const history = useHistory();
  const dispacth = useDispatch();
  const styles = useStyles();

  const page = useSelector((state: any) => state.page.page);

  const [open, setOpen] = useState(true);

  const handleClick = () => {
    setOpen(!open);
  };
  function handleListItemClick(link: string, index: number) {
    dispacth(pageRequest(index));
    history.push(`/school/${link}`);
  }

  return (
    <List component="nav" aria-labelledby="nested-list-subheader">
      <ListItemStyle
        selected={page === 2}
        button
        onClick={() => handleListItemClick('listClasses', 2)}
      >
        <ListItemIcon>
          <img src={IdIcon} alt="turmas" />
        </ListItemIcon>
        <ListItemText primary="Turmas" />
      </ListItemStyle>

      <ListItemStyle
        selected={page === 3}
        button
        onClick={() => handleListItemClick('listUsers', 3)}
      >
        <ListItemIcon>
          <img src={Group2} alt="turmas" />
        </ListItemIcon>
        <ListItemText primary="Usuários" />
      </ListItemStyle>

      <ListItemStyle
        selected={page === 4}
        button
        onClick={() => handleListItemClick('listImport', 4)}
      >
        <ListItemIcon>
          <img src={Download} alt="download" />
        </ListItemIcon>
        <ListItemText primary="Importar" />
      </ListItemStyle>
    </List>
  );
};

export default MenuSchool;
