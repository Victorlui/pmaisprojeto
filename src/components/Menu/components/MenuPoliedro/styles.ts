import styled from 'styled-components';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { ListItem } from '@material-ui/core';

export const Container = styled.div`
  background-color: white;
  box-shadow: 13px 1px 17px -7px rgba(0, 0, 0, 0.15);
  z-index: 1;
  padding: 30px 10px;
  width: 320px;

  @media (min-width: 481px) and (max-width: 767px) {
    display: none;
  }

  @media (min-width: 320px) and (max-width: 480px) {
    display: none;
  }

  @media (min-width: 768px) and (max-width: 1024px) {
    display: none;
  }
`;

export const ListItemStyle = withStyles({
  root: {
    borderRadius: 8,
    marginTop: 10,
    '&$selected': {
      backgroundColor: '#1B458D',
      color: 'white',
      '& .MuiListItemIcon-root': {
        color: 'white',
      },
      '& img': {
        filter: 'brightness(0) invert(1)',
      },
    },
    '&:hover': {
      backgroundColor: '#1B458D',
      color: 'white',
      '& .MuiListItemIcon-root': {
        color: 'white',
      },
      '& img': {
        filter: 'brightness(0) invert(1)',
      },
    },
  },
  selected: {},
})(ListItem);

export const useStyles = makeStyles(theme => ({
  nested: {
    paddingLeft: theme.spacing(4),
  },
}));
