import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { List, ListItemIcon, ListItemText, Collapse } from '@material-ui/core';

import { pageRequest } from '../../../../store/modules/page/actions';

import {
  IdIcon,
  Settings,
  Home,
  ArrowDown,
  Group,
} from '../../../../assets/icons';

import { Container, ListItemStyle, useStyles } from './styles';

const MenuPoliedro: React.FC = () => {
  const history = useHistory();
  const dispacth = useDispatch();
  const styles = useStyles();

  const page = useSelector((state: any) => state.page.page);

  const [open, setOpen] = useState(true);

  const handleClick = () => {
    setOpen(!open);
  };
  function handleListItemClick(link: string, index: number) {
    dispacth(pageRequest(index));
    history.push(`/poliedro/${link}`);
  }

  return (
    <List component="nav" aria-labelledby="nested-list-subheader">
      <ListItemStyle button>
        <ListItemIcon>
          <img src={Home} alt="home" />
        </ListItemIcon>
        <ListItemText primary="Home" />
      </ListItemStyle>
      <ListItemStyle button onClick={handleClick}>
        <ListItemIcon>
          <img src={Settings} alt="home" />
        </ListItemIcon>
        <ListItemText primary="Administração" />
        {open ? (
          <img src={ArrowDown} alt="arrow" />
        ) : (
          <img src={ArrowDown} alt="arrow" />
        )}
      </ListItemStyle>
      <Collapse in={open} timeout="auto" unmountOnExit>
        <List component="div">
          <ListItemStyle
            className={styles.nested}
            selected={page === 1}
            button
            onClick={() => handleListItemClick('listSchool', 1)}
          >
            <ListItemIcon>
              <img src={IdIcon} alt="home" />
            </ListItemIcon>
            <ListItemText primary="Lista de escolas" />
          </ListItemStyle>
          <ListItemStyle
            className={styles.nested}
            selected={page === 2}
            button
            onClick={() => handleListItemClick('listStudent', 2)}
          >
            <ListItemIcon>
              <img src={Group} alt="home" />
            </ListItemIcon>
            <ListItemText primary="Lista de usuários" />
          </ListItemStyle>
        </List>
      </Collapse>
    </List>
  );
};

export default MenuPoliedro;
