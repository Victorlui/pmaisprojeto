import { useRouteMatch } from 'react-router-dom';
import { useSelector } from 'react-redux';
import MenuPoliedro from './components/MenuPoliedro';
import MenuSchool from './components/MenuSchool';

import { Container } from './styles';

const Menu = () => {
  const type = useSelector((state: any) => state.auth.type);

  return (
    <Container>
      {type === '2' && <MenuSchool />}
      {type === '1' && <MenuPoliedro />}
    </Container>
  );
};

export default Menu;
