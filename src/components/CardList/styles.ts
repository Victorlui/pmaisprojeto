import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;

  /* MISC/white */

  background: #ffffff;
  /* Box-shadow-1 */

  box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
  border-radius: 8px;
  margin: 10px;
  width: 50%;
  height: 461px;

  @media (min-width: 481px) and (max-width: 767px) {
    width: 100%;
  }

  @media (min-width: 320px) and (max-width: 480px) {
    width: 100%;
  }

  @media (min-width: 768px) and (max-width: 1024px) {
    width: 100%;
  }
`;

export const Header = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  border-bottom: 1px solid #d3d3d3;
  padding: 10px;

  @media (min-width: 481px) and (max-width: 767px) {
    flex-direction: column;
    justify-content: center;
    align-items: flex-start;
  }

  @media (min-width: 320px) and (max-width: 480px) {
    flex-direction: column;
    justify-content: center;
    align-items: flex-start;
  }

  @media (min-width: 768px) and (max-width: 1024px) {
    flex-direction: column;
    justify-content: center;
    align-items: flex-start;
  }
`;

export const Title = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  width: 40%;

  p {
    font-family: Nunito;
    font-style: normal;
    font-weight: 600;
    font-size: 18px;
    line-height: 25px;
    margin: 10px;

    display: flex;
    align-items: center;

    /* grey-dark */

    color: rgba(0, 0, 0, 0.87);
  }
`;

export const BoxCheck = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  width: 100%;
`;

export const Content = styled.div`
  overflow: auto;
  display: inline;
  white-space: nowrap;

  /* for Firefox */
  min-height: 0;
  overflow-y: scroll;
  height: 461px;
  padding-left: 10px;
  padding-right: 10px;
  padding-bottom: 30px;
  margin-bottom: 30px;
`;

interface ItemProps {
  options: boolean;
}

export const Item = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: ${(props: ItemProps) => props.options && 'space-between'};
  border-bottom: 1px solid #d3d3d3;
  padding: 5px;
  margin: 10px;
  font-family: Nunito;
  font-style: normal;
  font-weight: normal;
  flex-grow: 1;
  flex: 1px;

  img {
    margin: 15px;
  }

  div {
    span {
      font-size: 14px;
      line-height: 19px;
      /* identical to box height */

      display: flex;
      align-items: center;

      /* Greys/grey_medium */

      color: rgba(0, 0, 0, 0.6);
    }
  }
`;

export const Description = styled.span`
  font-size: 16px;
  line-height: 22px;
  /* identical to box height */

  display: flex;
  text-align: center;

  /* Greys/grey_medium */

  color: rgba(0, 0, 0, 0.6);
`;

export const Footer = styled.footer`
  padding: 15px;
  border-top: 1px solid #d3d3d3;
`;
