import { CheckBox, Button } from 'pmaistemplates-v2';
import { Had, Group2, AddWhite, Delete, Edit } from '../../assets/icons';
import { Linha } from '../index';
import {
  Container,
  Header,
  Title,
  BoxCheck,
  Content,
  Item,
  Footer,
  Description,
} from './styles';

type Options = {
  id: number;
  title: string;
  type: number;
};

const initialOptions: Options[] = [
  {
    id: 1,
    title: 'Ano/ Vol/ Disciplina',
    type: 1,
  },
  {
    id: 2,
    title: 'Ano/ Vol/ Disciplina',
    type: 2,
  },
  {
    id: 3,
    title: 'Ano/ Vol/ Disciplina',
    type: 1,
  },
  {
    id: 4,
    title: 'Ano/ Vol/ Disciplina',
    type: 2,
  },
  {
    id: 5,
    title: 'Ano/ Vol/ Disciplina',
    type: 1,
  },
  {
    id: 6,
    title: 'Ano/ Vol/ Disciplina',
    type: 2,
  },
  {
    id: 7,
    title: 'Ano/ Vol/ Disciplina',
    type: 1,
  },
  {
    id: 8,
    title: 'Ano/ Vol/ Disciplina',
    type: 2,
  },
];

interface CardListProps {
  label: string;
  iconHeader: any;
  iconList: boolean;
  showCheck: boolean;
  showFooter: boolean;
  onClickFooter: () => any;
  optionsList: boolean;
}

const CardList = ({
  label,
  iconHeader,
  showCheck,
  iconList,
  showFooter,
  onClickFooter,
  optionsList,
}: CardListProps) => {
  return (
    <Container>
      <Header>
        <Title>
          <img src={iconHeader} alt="livro" />
          <p>{label}</p>
        </Title>
        {showCheck && (
          <BoxCheck>
            <CheckBox
              label="Aluno"
              id="aluno"
              onChange={() => {
                console.log('teste');
              }}
              name="teste"
              value="1"
              disabled={false}
              checked={false}
            />
            <CheckBox
              label="Professor"
              id="professor"
              onChange={() => {
                console.log('teste');
              }}
              name="teste"
              value="1"
              disabled={false}
              checked={false}
            />
          </BoxCheck>
        )}
      </Header>
      <Content>
        {initialOptions.map(i => (
          <Item key={i.id} options={optionsList}>
            {iconList && <img src={i.type === 1 ? Had : Group2} alt="icon" />}
            <div>
              <span>Nº do livro</span>
              <p>{i.title}</p>
            </div>
            {optionsList && (
              <>
                <Description>15/02/2021</Description>
                <Linha>
                  <img src={Delete} alt="teste" />
                  <img src={Edit} alt="teste" />
                </Linha>
              </>
            )}
          </Item>
        ))}
      </Content>
      {showFooter && (
        <Footer>
          <Button
            primary
            width="100%"
            round={false}
            onClick={() => {
              onClickFooter();
            }}
            label="Adicionar material extra"
            icon={AddWhite}
          />
        </Footer>
      )}
    </Container>
  );
};

export default CardList;
