import { Switch, Route } from 'react-router-dom';

import Dashboard from '../pages/TypePoliedro/Dashboard';
import SignIn from '../pages/SignIn';

import Create from '../pages/TypePoliedro/School/Create';
import ListSchool from '../pages/TypePoliedro/School/List';

/* livros */
import CreditBook from '../pages/TypePoliedro/Book/CreditBook';
import NewMaterial from '../pages/TypePoliedro/Book/NewMaterial';

/* Student */
import ListStudent from '../pages/TypePoliedro/Student/List';
import AddStudent from '../pages/TypePoliedro/Student/Create';

/* Type school */
import Turmas from '../pages/TypeSchool/Classes/List';
import TurmasCreate from '../pages/TypeSchool/Classes/Create';
import UsersList from '../pages/TypeSchool/Users/List';
import CreateUsers from '../pages/TypeSchool/Users/Create';
import ImportData from '../pages/TypeSchool/Import/ImportData';
import ListImport from '../pages/TypeSchool/Import/List';

const Routes = () => (
  <Switch>
    <Route path="/" exact component={SignIn} />
    <Route path="/poliedro/creditBook" component={CreditBook} />
    <Route path="/poliedro/dashboard" component={Dashboard} />
    <Route path="/poliedro/createSchool" component={Create} />
    <Route path="/poliedro/newMaterial" component={NewMaterial} />
    <Route path="/poliedro/listSchool" component={ListSchool} />
    <Route path="/poliedro/listStudent" component={ListStudent} />
    <Route path="/poliedro/addStudent" component={AddStudent} />

    <Route path="/school/listClasses" component={Turmas} />
    <Route path="/school/createClasses" component={TurmasCreate} />
    <Route path="/school/listUsers" component={UsersList} />
    <Route path="/school/createUsers" component={CreateUsers} />
    <Route path="/school/import" component={ImportData} />
    <Route path="/school/listImport" component={ListImport} />
  </Switch>
);
export default Routes;
