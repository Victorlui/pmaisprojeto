import { useHistory } from 'react-router-dom';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import InputAdornment from '@material-ui/core/InputAdornment';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import FormControl from '@material-ui/core/FormControl';
import Selects from '@material-ui/core/Select';

import { Button } from 'pmaistemplates-v2';
import { HeaderForm } from '../../../../components';
import { AddWhite, Search } from '../../../../assets/icons';

import Layout from '../../../Layout';
import TableUsers from '../../../../components/Tables/TableUsers';

import { Container, Content, Spacer, RowHeader } from './styles';

const data = [
  {
    id: 1,
    name: 'Lorem ipsum da Silva',
    user: 'Lorem',
    perfil: 'Aluno',
    status: '1',
  },
  {
    id: 2,
    name: 'Lorem ipsum Lima',
    user: 'Lorem',
    perfil: 'Professor',
    status: '0',
  },
];

const List = () => {
  const history = useHistory();

  const handleCreate = () => {
    history.push('/school/createUsers');
  };

  return (
    <Layout>
      <Container>
        <HeaderForm
          active={1}
          title="Usuários"
          tabs={false}
          buttonTop
          subTitle=""
        >
          <Button
            label="Novo usuário"
            width="170px"
            icon={AddWhite}
            onClick={handleCreate}
          />
        </HeaderForm>

        <Content>
          <RowHeader>
            <OutlinedInput
              style={{
                width: '60%',
                margin: '10px',
                background: 'white',
                height: 48,
              }}
              placeholder="Pesquisar"
              endAdornment={
                <InputAdornment position="end">
                  <img alt="search" src={Search} />
                </InputAdornment>
              }
            />

            <Spacer />

            <FormControl
              variant="outlined"
              style={{ width: '30%', height: 48, margin: '10px' }}
            >
              <InputLabel style={{ top: -3 }}>Perfil</InputLabel>
              <Selects
                style={{
                  width: '100%',
                  background: 'white',
                  height: 48,
                }}
                label="Perfil"
              >
                <MenuItem value={10}>Ten</MenuItem>
                <MenuItem value={20}>Twenty</MenuItem>
                <MenuItem value={30}>Thirty</MenuItem>
              </Selects>
            </FormControl>

            <FormControl
              variant="outlined"
              style={{ width: '30%', height: 48, margin: '10px' }}
            >
              <InputLabel style={{ top: -3 }}>Status</InputLabel>
              <Selects
                style={{
                  width: '100%',
                  background: 'white',
                  height: 48,
                }}
                label="Status"
              >
                <MenuItem value={10}>Ten</MenuItem>
                <MenuItem value={20}>Twenty</MenuItem>
                <MenuItem value={30}>Thirty</MenuItem>
              </Selects>
            </FormControl>
          </RowHeader>
          <TableUsers
            data={data}
            limit={20}
            pagesNavigation={2}
            totalItems={100}
            onChangeLimit={value => {
              console.tron.log('teste');
            }}
          />
        </Content>
      </Container>
    </Layout>
  );
};

export default List;
