import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  background-color: #fafbfc;
`;

export const Content = styled.div`
  padding: 32px;
`;

export const RowHeader = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const Spacer = styled.hr`
  border: 0.6px solid rgba(0, 0, 0, 0.12);
  height: 48px;
`;
