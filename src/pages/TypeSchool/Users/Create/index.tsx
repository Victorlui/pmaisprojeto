import React from 'react';
import { Button } from 'pmaistemplates-v2';
import { HeaderForm } from '../../../../components';
import { GroupWhite } from '../../../../assets/icons';

import Layout from '../../../Layout';
import Cadastro from '../components/Cadastro';

import { Container } from './styles';

const Create: React.FC = () => {
  return (
    <Layout>
      <Container>
        <HeaderForm
          active={0}
          title="Nome"
          subTitle="Modificado em: 13/03/2021"
          tabs
          titleActive1="Cadastro"
          titleActive2="Dependentes"
          buttonTop
        >
          <Button
            width="200px"
            label="Impersonar"
            icon={GroupWhite}
            onClick={() => {
              console.log('teste');
            }}
          />
        </HeaderForm>

        <Cadastro />
      </Container>
    </Layout>
  );
};

export default Create;
