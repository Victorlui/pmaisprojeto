import React from 'react';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import { UploadFiles } from 'pmaistemplates-v2';

import { Edit, GroupWhite, Key, Save } from '../../../../../assets/icons';

import TableHistoric from '../../../../../components/Tables/TableHistoric';

import {
  Container,
  Row,
  useStyles,
  Column,
  ButtonKey,
  ButtonEdit,
  Spacer,
  Historico,
} from './styles';

import { Divider } from '../../../../../components/Header/styles';
import { Footer } from '../../../../../components';

const list = [
  {
    id: 1,
    unidade: 'Cidade 1 - Cólegio P+',
    turma: 'Turma lorem ipsum',
    curso: 'EFAF-9º ano',
    disciplina: 'Lorem ipsum',
    status: '1',
  },
];

const Cadastro: React.FC = () => {
  const classes = useStyles();
  return (
    <Container>
      <h1>Cadastro</h1>
      <Spacer />
      <Row>
        <Column style={{ width: 250 }}>
          <UploadFiles
            handle={() => {
              console.log('upload');
            }}
            title="Adicionar imagem"
            subtitle="Ou arraste a imagem aqui"
          />
        </Column>

        <Column>
          <Row>
            <TextField
              className={classes.root}
              label="Nome"
              variant="outlined"
            />

            <FormControl
              variant="outlined"
              className={classes.root}
              style={{ width: '50%' }}
            >
              <InputLabel>Status</InputLabel>
              <Select label="Status">
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={10}>Ten</MenuItem>
                <MenuItem value={20}>Twenty</MenuItem>
                <MenuItem value={30}>Thirty</MenuItem>
              </Select>
            </FormControl>
          </Row>

          <Row>
            <TextField
              style={{ width: '90%', marginTop: 10 }}
              className={classes.root}
              label="E-mail P4ED"
              variant="outlined"
            />
            <ButtonKey>
              <img src={Key} alt="key" />
            </ButtonKey>
          </Row>
        </Column>
      </Row>

      <Row>
        <TextField
          style={{ width: 250, marginTop: 20, backgroundColor: '#fafbfc' }}
          label="E-mail"
          variant="outlined"
        />
        <TextField
          style={{ width: 200, marginTop: 20, backgroundColor: '#fafbfc' }}
          label="Telefone"
          variant="outlined"
        />
        <TextField
          style={{ width: 200, marginTop: 20, backgroundColor: '#fafbfc' }}
          label="CPF"
          variant="outlined"
        />

        <FormControl
          variant="outlined"
          style={{ width: 290, marginTop: 20, backgroundColor: '#fafbfc' }}
        >
          <InputLabel>Acessibilidade</InputLabel>
          <Select label="Acessibilidade">
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            <MenuItem value={10}>Ten</MenuItem>
            <MenuItem value={20}>Twenty</MenuItem>
            <MenuItem value={30}>Thirty</MenuItem>
          </Select>
        </FormControl>
      </Row>
      <hr />
      <Spacer />
      <h1>Perfil e permissões</h1>
      <Spacer />
      <Column>
        <Row>
          <FormControl
            variant="outlined"
            style={{ width: 270, backgroundColor: '#fafbfc' }}
          >
            <InputLabel>Perfil</InputLabel>
            <Select label="Perfil">
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              <MenuItem value={10}>Ten</MenuItem>
              <MenuItem value={20}>Twenty</MenuItem>
              <MenuItem value={30}>Thirty</MenuItem>
            </Select>
          </FormControl>

          <TextField
            style={{ width: '50%', backgroundColor: '#fafbfc' }}
            label="Cargo"
            variant="outlined"
          />

          <ButtonEdit type="button">
            <img src={Edit} alt="group" />
            Editar permissões
          </ButtonEdit>
        </Row>
        <button type="button">
          <img src={GroupWhite} alt="group" />
          Adicionar perfil
        </button>
      </Column>

      <hr />
      <Spacer />
      <Historico>
        <h1>Historico</h1>
        <FormControl
          variant="outlined"
          style={{
            width: 150,
            backgroundColor: '#fafbfc',
            marginLeft: 10,
            height: 48,
          }}
        >
          <InputLabel>Ano Letivo</InputLabel>
          <Select label="Ano Letivo" style={{ height: 48 }}>
            <MenuItem value={10}>2021</MenuItem>
            <MenuItem value={20}>2020</MenuItem>
            <MenuItem value={30}>2019</MenuItem>
          </Select>
        </FormControl>
      </Historico>

      <Spacer />
      <TableHistoric
        data={list}
        limit={20}
        pagesNavigation={2}
        totalItems={100}
        onChangeLimit={value => {
          console.tron.log('teste');
        }}
      />

      <Divider />

      <Footer>
        <button id="btn-cancelar" type="button">
          Cancelar
        </button>
        <button id="btn-salvar" type="button">
          <img src={Save} alt="save" />
          <p>Salvar</p>
        </button>
      </Footer>
    </Container>
  );
};

export default Cadastro;
