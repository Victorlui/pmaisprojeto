import styled from 'styled-components';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      backgroundColor: '#fafbfc',
      width: '100%',
      marginLeft: 10,
    },
  }),
);

export const Container = styled.div`
  background: #ffffff;
  box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
  border-radius: 8px;
  margin: 30px;
  padding-top: 20px;
  padding-left: 15px;
  padding-right: 15px;
  padding-bottom: 20px;

  h1 {
    font-family: 'Nunito';
    font-style: normal;
    font-weight: 600;
    font-size: 24px;
    line-height: 33px;
    color: rgba(0, 0, 0, 0.87);
  }

  hr {
    border: 1px solid rgba(0, 0, 0, 0.12);
    height: 0.6px;
    width: 100%;
    margin-top: 20px;
    margin-bottom: 20px;
  }
`;

export const Row = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
`;

export const Column = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 100%;

  > button {
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    background: #1b458d;
    border: 0px;
    color: #fff;
    border-radius: 8px;
    padding: 10px;
    width: 176px;
    height: 48px;
    margin-top: 20px;

    :hover {
      filter: brightness(0.9);
    }

    img {
      margin-right: 10px;
      object-fit: cover;
    }
  }
`;

export const Spacer = styled.div`
  margin: 40px;
`;

export const ButtonKey = styled.button`
  background: #ffffff;
  border: 2px solid #1b458d;
  box-sizing: border-box;
  border-radius: 8px;
  width: 55px;
  height: 55px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin-top: 10px;
  margin-left: 10px;

  :hover {
    filter: brightness(0.9);
  }
`;

export const ButtonEdit = styled.button`
  background: #ffffff;
  border: 2px solid #1b458d;
  box-sizing: border-box;
  border-radius: 8px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  padding: 10px;
  font-family: Nunito;
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 22px;
  color: #1b458d;

  :hover {
    filter: brightness(0.9);
  }

  img {
    margin-right: 10px;
    object-fit: cover;
  }
`;

export const Historico = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;

  h1 {
    font-family: 'Nunito', sans-serif;
    font-style: normal;
    font-weight: 600;
    font-size: 24px;
    line-height: 33px;

    color: rgba(0, 0, 0, 0.87);
  }
`;
