import styled from 'styled-components';

export const Fundo = styled.div`
  background-color: #fafbfc;
  padding: 30px;
`;

export const Container = styled.div`
  width: 100%;
  background-color: #fafbfc;
`;

export const TitleBox = styled.div`
  margin-left: 20px;
  display: flex;

  @media (min-width: 481px) and (max-width: 767px) {
    margin-left: 0px;
  }

  @media (min-width: 320px) and (max-width: 480px) {
    margin-left: 0px;
  }
`;

export const RowSchool = styled.div`
  display: flex;
  flex-direction: row;
`;

export const ColumnSchool = styled.div`
  width: 50%;
`;
