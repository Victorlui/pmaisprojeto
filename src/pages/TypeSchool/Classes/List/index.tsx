import { useState } from 'react';
import { Button, Row } from 'pmaistemplates-v2';
import { useHistory } from 'react-router-dom';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import InputAdornment from '@material-ui/core/InputAdornment';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import FormControl from '@material-ui/core/FormControl';
import Selects from '@material-ui/core/Select';

import TableClasses from '../../../../components/Tables/TableClasses';

import { HeaderForm } from '../../../../components';
import { Container, Form, Spacer } from './styles';
import { AddWhite, Search } from '../../../../assets/icons';
import Layout from '../../../Layout';

const schoolsList = [
  {
    id: 1,
    nameTurma: 'Lorem ipsum',
    segmento: 'EFAI',
    serie: '4º Ano',
    curso: 'Padrão',
    licencas: '50/50',
  },
  {
    id: 2,
    nameTurma: 'Lorem ipsum',
    segmento: 'EFAI',
    serie: '4º Ano',
    curso: 'Padrão',
    licencas: '50/50',
  },
  {
    id: 3,
    nameTurma: 'Lorem ipsum',
    segmento: 'EM',
    serie: '1º Série',
    curso: 'Complementar',
    licencas: '55/50',
  },
];

const List = () => {
  const history = useHistory();

  const handleCreate = () => {
    history.push('/school/createClasses');
  };

  return (
    <Layout>
      <Container>
        <HeaderForm
          active={1}
          title="Turmas"
          tabs={false}
          buttonTop
          subTitle=""
        >
          <Button
            label="Nova Turma"
            width="170px"
            icon={AddWhite}
            onClick={() => {
              handleCreate();
            }}
          />
        </HeaderForm>
        <Form>
          <Row>
            <OutlinedInput
              style={{
                width: '60%',
                margin: '10px',
                background: 'white',
                height: 48,
              }}
              placeholder="Pesquisar"
              endAdornment={
                <InputAdornment position="end">
                  <img alt="search" src={Search} />
                </InputAdornment>
              }
            />

            <Spacer />

            <FormControl
              variant="outlined"
              style={{ width: '30%', height: 48, margin: '10px' }}
            >
              <InputLabel style={{ top: -3 }}>Segmento</InputLabel>
              <Selects
                style={{
                  width: '100%',
                  background: 'white',
                  height: 48,
                }}
                label="Segmento"
              >
                <MenuItem value={10}>Ten</MenuItem>
                <MenuItem value={20}>Twenty</MenuItem>
                <MenuItem value={30}>Thirty</MenuItem>
              </Selects>
            </FormControl>

            <FormControl
              variant="outlined"
              style={{ width: '30%', height: 48, margin: '10px' }}
            >
              <InputLabel style={{ top: -3 }}>Série/Ano</InputLabel>
              <Selects
                style={{
                  width: '100%',
                  background: 'white',
                  height: 48,
                }}
                label="Série/Ano"
              >
                <MenuItem value={10}>Ten</MenuItem>
                <MenuItem value={20}>Twenty</MenuItem>
                <MenuItem value={30}>Thirty</MenuItem>
              </Selects>
            </FormControl>
          </Row>

          <TableClasses
            data={schoolsList}
            limit={20}
            pagesNavigation={2}
            totalItems={100}
            onChangeLimit={value => {
              console.tron.log('teste');
            }}
          />
        </Form>
      </Container>
    </Layout>
  );
};

export default List;
