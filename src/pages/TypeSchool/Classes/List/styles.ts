import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  background-color: #fafbfc;
`;

export const Form = styled.div`
  padding: 20px;
`;

export const Spacer = styled.hr`
  border: 0.6px solid rgba(0, 0, 0, 0.12);
  height: 48px;
`;

export const LoadingBox = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 37vh;
`;
