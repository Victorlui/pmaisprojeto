import React from 'react';
import { useHistory } from 'react-router-dom';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import FormControl from '@material-ui/core/FormControl';
import InputAdornment from '@material-ui/core/InputAdornment';
import TableAlunos from '../../../../../components/Tables/TableAlunos';

import {
  Search,
  Profile,
  AddWhite,
  Options,
} from '../../../../../assets/icons';
import { Container, Row, Button, Option, Spacer } from './styles';

const classesList = [
  {
    id: 1,
    name: 'Lorem ipsum da Silva',
    user: 'XXXXXXXXXX',
    matricula: '1234567890',
    status: '1',
  },
  {
    id: 2,
    name: 'Lorem ipsum da Silva',
    user: 'XXXXXXXXXX',
    matricula: '1234567890',
    status: '0',
  },
];

const Licencas: React.FC = () => {
  return (
    <Container>
      <Row>
        <FormControl
          variant="outlined"
          style={{ backgroundColor: '#fff', width: '60%' }}
        >
          <OutlinedInput
            placeholder="Nome do aluno"
            style={{ height: 48 }}
            endAdornment={
              <InputAdornment position="end">
                <img src={Search} alt="Search" />
              </InputAdornment>
            }
            startAdornment={
              <InputAdornment position="start">
                <img src={Profile} alt="Search" />
              </InputAdornment>
            }
          />
        </FormControl>
        <Button>
          <img src={AddWhite} alt="teste" />
          Adicionar
        </Button>
        <p>Créditos disponiveis: 38</p>
        <Spacer />
        <Option>
          <img src={Options} alt="options" />
        </Option>
      </Row>
      <TableAlunos
        data={classesList}
        limit={20}
        pagesNavigation={2}
        totalItems={10}
        onChangeLimit={value => {
          console.tron.log('aqui');
        }}
      />
    </Container>
  );
};

export default Licencas;
