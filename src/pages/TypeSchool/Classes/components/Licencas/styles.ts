import styled from 'styled-components';

export const Container = styled.div`
  margin-left: 20px;
  margin-right: 20px;
  margin-top: 20px;

  @media (min-width: 481px) and (max-width: 767px) {
    margin-left: 10px;
    margin-right: 10px;
  }

  @media (min-width: 320px) and (max-width: 480px) {
    margin-left: 10px;
    margin-right: 10px;
  }

  @media (min-width: 768px) and (max-width: 1024px) {
    margin-left: 10px;
    margin-right: 10px;
  }
`;

export const Row = styled.div`
  display: flex;
  align-items: center;
  flex-direction: row;
  justify-content: space-between;
  margin-left: 20px;
  margin-right: 20px;

  p {
    font-family: Nunito;
    font-style: normal;
    font-weight: 600;
    font-size: 16px;
    line-height: 22px;
    color: rgba(0, 0, 0, 0.6);
  }

  @media (min-width: 481px) and (max-width: 767px) {
    margin-left: 10px;
    margin-right: 10px;
    flex-direction: column;
  }

  @media (min-width: 320px) and (max-width: 480px) {
    margin-left: 10px;
    margin-right: 10px;
    flex-direction: column;
  }

  @media (min-width: 768px) and (max-width: 1024px) {
    margin-left: 10px;
    margin-right: 10px;
    flex-direction: column;
  }
`;

export const Button = styled.button`
  display: flex;
  align-items: center;
  flex-direction: row;
  justify-content: center;
  color: #fff;
  background: #1b458d;
  border: 0;
  border-radius: 8px;
  width: 134px;
  height: 48px;
  font-family: Nunito;
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 22px;

  :hover {
    background-color: #14346b;
    color: white;
  }

  img {
    margin-right: 10px;
  }
`;

export const Option = styled.button`
  background: #ffffff;
  border: 2px solid #1b458d;
  box-sizing: border-box;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 8px;
  width: 48px;
  height: 48px;
`;

export const Spacer = styled.hr`
  border: 0.6px solid rgba(0, 0, 0, 0.12);
  height: 35px;
`;
