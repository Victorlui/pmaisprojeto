import React, { useState, useEffect } from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import DateFnsUtils from '@date-io/date-fns';
import Buttons from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import { Title, Button } from 'pmaistemplates-v2';
import {
  Check,
  Close,
  Calendar,
  AddWhite,
  Save,
  DeleteGray,
  Edit,
  Search,
  CloseBlue,
  Info,
  Profile,
} from '../../../../../assets/icons';

import {
  Input,
  Linha,
  TitleForm,
  useStyles,
  Divider,
  RowHeader,
  RowTitle,
  TableForm,
  ButtonOptions,
  Footer,
} from '../../../../../components';

import { ModalHeader, ModalFooter, ButtonProf, RowProf } from './styles';

const head = [
  {
    id: 1,
    title: 'Livro Aluno',
  },
  {
    id: 2,
    title: 'Disciplina',
  },
  {
    id: 3,
    title: 'Professor',
  },
];

const head2 = [
  {
    id: 1,
    title: 'Cordenadores',
  },
];

const data1 = [
  {
    id: 1,
    livro: 'Livro 1',
    title: 'Literatura',
    professor: 'Ronald Richards',
  },
  {
    id: 2,
    livro: 'Livro 1',
    title: 'Literatura',
    professor: 'Ronald Richards',
  },
  {
    id: 3,
    livro: 'Livro 1',
    title: 'Literatura',
    professor: 'Ronald Richards',
  },
];

const data2 = [
  {
    id: 1,
    livro: 'Ronald Richards',
  },
];

const Cadastro: React.FC = () => {
  const classes = useStyles();
  const classesInput = useStyles();

  const [selectedDate, setSelectedDate] = useState<Date | null>(
    new Date('2014-08-18T21:11:54'),
  );

  const [array, setArray] = useState<any[]>([]);
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(!open);
  };

  const handleDateChange = (date: Date | null) => {
    setSelectedDate(date);
  };

  return (
    <div>
      <TitleForm>
        <Title size="medium">Dados gerais</Title>
      </TitleForm>

      <Linha>
        <Input label="Nome Turma" id="name" type="text" variant="outlined" />

        <FormControl variant="outlined" className={classes.formControl}>
          <InputLabel>Tipo do Curso</InputLabel>
          <Select label="Tipo do Curso">
            <MenuItem value={10}>Ten</MenuItem>
            <MenuItem value={20}>Twenty</MenuItem>
            <MenuItem value={30}>Thirty</MenuItem>
          </Select>
        </FormControl>

        <FormControl variant="outlined" className={classes.formControl}>
          <InputLabel>Status</InputLabel>
          <Select label="Status">
            <MenuItem value={10}>
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}
              >
                <img src={Check} alt="check" style={{ marginRight: 5 }} />
                Ativo
              </div>
            </MenuItem>
            <MenuItem value={20}>
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  alignItems: 'center',
                }}
              >
                <img src={Close} alt="check" style={{ marginRight: 5 }} />
                Inativo
              </div>
            </MenuItem>
          </Select>
        </FormControl>
      </Linha>

      <Linha>
        <FormControl variant="outlined" className={classes.formControl}>
          <InputLabel>Segmento</InputLabel>
          <Select label="Segmento">
            <MenuItem value={10}>Ten</MenuItem>
            <MenuItem value={20}>Twenty</MenuItem>
            <MenuItem value={30}>Thirty</MenuItem>
          </Select>
        </FormControl>

        <FormControl variant="outlined" className={classes.formControl}>
          <InputLabel>Ano/Série</InputLabel>
          <Select label="Ano/Série">
            <MenuItem value={10}>Ten</MenuItem>
            <MenuItem value={20}>Twenty</MenuItem>
            <MenuItem value={30}>Thirty</MenuItem>
          </Select>
        </FormControl>
        <FormControl variant="outlined" className={classes.formControl}>
          <InputLabel>Coleção</InputLabel>
          <Select label="Coleção">
            <MenuItem value={10}>Ten</MenuItem>
            <MenuItem value={20}>Twenty</MenuItem>
            <MenuItem value={30}>Thirty</MenuItem>
          </Select>
        </FormControl>

        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <FormControl variant="outlined" className={classesInput.formControl}>
            <InputLabel>Ínicio do Ano Letivo</InputLabel>
            <OutlinedInput
              label="Ínicio do Ano Letivo"
              endAdornment={
                <InputAdornment position="end" style={{ cursor: 'pointer' }}>
                  <img src={Calendar} alt="check" />
                </InputAdornment>
              }
            />
          </FormControl>
        </MuiPickersUtilsProvider>
      </Linha>

      <Divider />

      <TitleForm>
        <RowHeader>
          <RowTitle>
            <Title size="medium">Disciplinas</Title>
          </RowTitle>
          <Button
            round={false}
            icon={AddWhite}
            width="200px"
            label="Adicionar disciplina"
            onClick={handleClickOpen}
          />
        </RowHeader>
        <TableForm data={data1} head={head}>
          {data1.map((d: any) => (
            <tr>
              <td>{d.livro}</td>
              <td>{d.title}</td>
              <td>{d.professor}</td>
              <td colSpan={10} align="right">
                <ButtonOptions>
                  <img src={Edit} alt="edit" />
                </ButtonOptions>
                <ButtonOptions>
                  <img src={DeleteGray} alt="delete" />
                </ButtonOptions>
              </td>
            </tr>
          ))}
        </TableForm>
      </TitleForm>

      <Divider />

      <TitleForm>
        <RowHeader>
          <Title size="medium">Coordenadores</Title>
          <Button
            round={false}
            icon={AddWhite}
            width="230px"
            label="Adicionar Coordenador"
            onClick={() => console.log('aqui')}
          />
        </RowHeader>
        <TableForm data={data2} head={head2}>
          {data2.map(d => (
            <tr>
              <td>{d.livro}</td>
              <td align="right">
                <ButtonOptions>
                  <img src={Edit} alt="edit" />
                </ButtonOptions>
                <ButtonOptions>
                  <img src={DeleteGray} alt="delete" />
                </ButtonOptions>
              </td>
            </tr>
          ))}
        </TableForm>
      </TitleForm>

      <Divider />

      <Footer>
        <button id="btn-cancelar" type="button">
          Cancelar
        </button>
        <button id="btn-salvar" type="button">
          <img src={Save} alt="send" />
          <p>Salvar</p>
        </button>
      </Footer>

      <Dialog open={open} onClose={handleClickOpen}>
        <div style={{ width: 500 }}>
          <ModalHeader>
            <p>Adicionar disciplina</p>
          </ModalHeader>
          <DialogContent>
            <DialogContentText>
              <FormControl
                variant="outlined"
                style={{
                  backgroundColor: '#fff',
                  width: '100%',
                  marginTop: 20,
                }}
              >
                <InputLabel>Tipo do Curso</InputLabel>
                <Select label="Tipo do Curso">
                  <MenuItem value={10}>Ten</MenuItem>
                  <MenuItem value={20}>Twenty</MenuItem>
                  <MenuItem value={30}>Thirty</MenuItem>
                </Select>
              </FormControl>

              <FormControl
                variant="outlined"
                style={{
                  backgroundColor: '#fff',
                  width: '100%',
                  marginTop: 20,
                }}
              >
                <InputLabel htmlFor="outlined-adornment-password">
                  Nome da disciplina{' '}
                </InputLabel>
                <OutlinedInput
                  label="Nome da disciplina"
                  id="name"
                  type="text"
                  style={{ margin: 0 }}
                />
              </FormControl>

              <FormControl
                variant="outlined"
                style={{
                  backgroundColor: '#fff',
                  width: '100%',
                  marginTop: 20,
                }}
              >
                <InputLabel htmlFor="outlined-adornment-password">
                  Professor
                </InputLabel>
                <OutlinedInput
                  label="Professor"
                  id="name"
                  type="text"
                  endAdornment={
                    <InputAdornment position="end">
                      <img src={Search} alt="teste" />
                    </InputAdornment>
                  }
                  style={{ margin: 0 }}
                />
              </FormControl>

              <RowProf>
                <ButtonProf>
                  <img src={Profile} alt="Profile" />
                  <p>Professor</p>
                  <img src={Close} alt="Close" />
                </ButtonProf>
                <ButtonProf>
                  <img src={Profile} alt="Profile" />
                  <p>Professor</p>
                  <img src={Close} alt="Close" />
                </ButtonProf>
              </RowProf>
            </DialogContentText>
          </DialogContent>
          <ModalFooter>
            <Buttons onClick={handleClickOpen} color="primary">
              Cancelar
            </Buttons>
            <Buttons onClick={handleClickOpen} color="primary" autoFocus>
              Adicionar
            </Buttons>
          </ModalFooter>
        </div>
      </Dialog>
    </div>
  );
};

export default Cadastro;
