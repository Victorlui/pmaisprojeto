import styled from 'styled-components';

export const ModalHeader = styled.div`
  border-bottom: 1px solid rgba(0, 0, 0, 0.12);
  padding: 20px;

  p {
    font-family: Nunito;
    font-style: normal;
    font-weight: 600;
    font-size: 24px;
    line-height: 33px;
    color: rgba(0, 0, 0, 0.87);
  }
`;

export const ModalContent = styled.div``;

export const ModalFooter = styled.div`
  border-top: 1px solid rgba(0, 0, 0, 0.12);
  padding: 20px;
  display: flex;
  align-items: flex-end;
  justify-content: flex-end;
`;

export const RowProf = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  flex-wrap: wrap;
`;

export const ButtonProf = styled.div`
  background: #e3eeff;
  border: 2px solid #1b458d;
  box-sizing: border-box;
  border-radius: 50px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  width: 209px;
  height: 46px;
  padding: 10px;
  margin-top: 20px;
  margin-right: 17px;

  p {
    font-family: Nunito;
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    line-height: 22px;

    text-decoration-line: underline;

    color: #1b458d;
  }

  img {
    cursor: pointer;
  }
`;
