import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  background-color: #f9f9f9;
  width: 100%;

  aside {
    height: 108px;
    background-color: #fff;
    padding: 32px;
    border-bottom: 1px solid rgba(0, 0, 0, 0.12);
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;

    button {
      background: #1b458d;
      border-radius: 50px;
      border: 0;
      display: flex;
      flex-direction: row;
      align-items: center;
      justify-content: center;
      color: #fff;
      padding: 10px;
      width: 150px;

      img {
        margin-right: 10px;
      }
    }

    h1 {
      font-family: Nunito;
      font-style: normal;
      font-weight: 600;
      font-size: 32px;
      line-height: 44px;
      color: rgba(0, 0, 0, 0.87);
    }
  }
`;

export const Content = styled.div`
  padding: 32px;
`;
