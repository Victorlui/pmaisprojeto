import React from 'react';
import { useHistory } from 'react-router-dom';
import { Cloud } from '../../../../assets/icons';

import Layout from '../../../Layout';
import TableImport from '../../../../components/Tables/TableImport';

import { Container, Content } from './styles';

const data = [
  {
    id: 1,
    name: 'arquivo.xls',
    data: '05/10/2021 - 15:12',
    carregado: 'Lorem ipsum',
  },
  {
    id: 2,
    name: 'arquivo.xls',
    data: '05/10/2021 - 15:12',
    carregado: 'Lorem ipsum',
  },
  {
    id: 3,
    name: 'arquivo.xls',
    data: '05/10/2021 - 15:12',
    carregado: 'Lorem ipsum',
  },
  {
    id: 4,
    name: 'arquivo.xls',
    data: '05/10/2021 - 15:12',
    carregado: 'Lorem ipsum',
  },
  {
    id: 5,
    name: 'arquivo.xls',
    data: '05/10/2021 - 15:12',
    carregado: 'Lorem ipsum',
  },
  {
    id: 6,
    name: 'arquivo.xls',
    data: '05/10/2021 - 15:12',
    carregado: 'Lorem ipsum',
  },
];

const List: React.FC = () => {
  const history = useHistory();

  const handlePage = () => {
    history.push('/school/import');
  };

  return (
    <Layout>
      <Container>
        <aside>
          <h1>Histórico</h1>

          <button onClick={handlePage} type="button">
            <img src={Cloud} alt="import" />
            Importar
          </button>
        </aside>

        <Content>
          <TableImport
            data={data}
            limit={10}
            pagesNavigation={1}
            totalItems={100}
            onChangeLimit={value => {
              console.tron.log(value);
            }}
          />
        </Content>
      </Container>
    </Layout>
  );
};

export default List;
