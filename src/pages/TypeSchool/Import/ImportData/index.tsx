import { useState } from 'react';
import { UploadFiles } from 'pmaistemplates-v2';
import { Download, History, Send } from '../../../../assets/icons';
import { Footer } from '../../../../components';

import Layout from '../../../Layout';

import { Container, Steps, Step1, Step2, Step3 } from './styles';

const ImportData = () => {
  const [active, setActive] = useState(1);

  function handleActive(id: number) {
    setActive(id);
  }

  return (
    <Layout>
      <Container>
        <aside>
          <h1>Importador</h1>
        </aside>

        <Steps>
          <div>
            <button type="button" id="step" onClick={() => handleActive(1)}>
              1
            </button>
            <p>Carregar dados</p>
          </div>
          <hr />
          <div>
            <button type="button" id="step" onClick={() => handleActive(2)}>
              2
            </button>
            <p>Verificar erros</p>
          </div>
          <hr />
          <div>
            <button type="button" id="step" onClick={() => handleActive(3)}>
              3
            </button>
            <p>Confirmar e importar</p>
          </div>
        </Steps>

        {active === 1 && (
          <Step1>
            <div>
              <h1>Envie sua planilha</h1>
              <span>
                Faça o download da planilha modelo clicando no botão abaixo e
                preencha as linhas solicitadas.
              </span>
            </div>

            <hr />

            <button type="button">
              <img src={Download} alt="download" />
              Download modelo
            </button>

            <UploadFiles
              handle={() => {
                console.log('upload');
              }}
              title="Adicionar imagem"
              subtitle="Ou arraste a imagem aqui"
            />

            <div id="history">
              <img src={History} alt="history" />
              Histórico
            </div>
          </Step1>
        )}

        {active === 2 && (
          <>
            <Step2>
              <div>
                <h3>Alunos</h3>
                <p>224 encontrados</p>
                <span>17 Erros encontrados</span>
              </div>
              <div>
                <h3>Responsáveis</h3>
                <p>224 encontrados</p>
                <span>17 Erros encontrados</span>
              </div>
            </Step2>
            <Step1>
              <div>
                <h1>Corrija sua planilha</h1>
                <div id="btn-download">
                  <span>
                    Faça o download da planilha clicando no botão abaixo e
                    confira as linhas em vermelho de cada aba. Corrija as
                    retificações solicitadas na última coluna.
                  </span>
                  <button type="button">
                    <img src={Download} alt="download" />
                    Baixar planilha
                  </button>
                </div>
              </div>

              <UploadFiles
                handle={() => {
                  console.log('upload');
                }}
                title="Adicionar imagem"
                subtitle="Ou arraste a imagem aqui"
              />
            </Step1>
          </>
        )}

        {active === 3 && (
          <>
            <Step3>
              <div id="header">
                <h3>Todos os dados foram validados com sucesso</h3>
                <span>
                  Confira atentamente os dados abaixo antes de confirmar a
                  atualização
                </span>
              </div>

              <div id="content">
                <div id="dados">
                  <div>
                    <h3>Turmas</h3>
                    <p>12 Turmas serão atualizadas</p>
                    <span>1 Turma será cadastrada</span>
                  </div>
                  <div>
                    <h3>Alunos</h3>
                    <p>12 Turmas serão atualizadas</p>
                    <span>1 Turma será cadastrada</span>
                  </div>
                  <div>
                    <h3>Colaboradores</h3>
                    <p>12 Turmas serão atualizadas</p>
                    <span>1 Turma será cadastrada</span>
                  </div>
                </div>
                <hr />
                <Footer>
                  <button id="btn-cancelar" type="button">
                    Cancelar
                  </button>
                  <button id="btn-salvar" type="button">
                    <img src={Send} alt="send" />
                    <p>Importar</p>
                  </button>
                </Footer>
              </div>
            </Step3>
          </>
        )}
      </Container>
    </Layout>
  );
};

export default ImportData;
