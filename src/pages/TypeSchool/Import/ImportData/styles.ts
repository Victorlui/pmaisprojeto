import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  background-color: #f9f9f9;
  width: 100%;

  aside {
    height: 108px;
    background-color: #fff;
    padding: 32px;
    border-bottom: 1px solid rgba(0, 0, 0, 0.12);

    h1 {
      font-family: Nunito;
      font-style: normal;
      font-weight: 600;
      font-size: 32px;
      line-height: 44px;
      color: rgba(0, 0, 0, 0.87);
    }
  }
`;

export const Steps = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding: 32px;
  width: 100%;

  hr {
    width: 100px;
    height: 4px;
    background-color: #c2c2c2;
    border: 0;
  }

  > div {
    display: flex;
    flex-direction: row;
    align-items: center;

    #step {
      width: 60px;
      height: 60px;
      background: #1b458d;
      border: 1px solid #1b458d;
      box-sizing: border-box;
      border-radius: 50%;
      display: flex;
      justify-content: center;
      align-items: center;
      color: #fff;
      font-family: Nunito;
      font-style: normal;
      font-weight: 600;
      font-size: 32px;
      line-height: 48px;
      cursor: pointer;
    }

    p {
      font-family: Nunito;
      font-style: normal;
      font-weight: 600;
      font-size: 20px;
      line-height: 28px;
      color: #1b458d;
      margin-left: 10px;
    }
  }
`;

export const Step1 = styled.div`
  background: #ffffff;
  box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
  border-radius: 8px;
  margin-left: 30px;
  margin-right: 30px;
  margin-bottom: 30px;
  padding: 20px;

  hr {
    border: 1px solid rgba(0, 0, 0, 0.12);
    margin-top: 20px;
    margin-bottom: auto;
  }

  > div {
    font-family: 'Nunito';
    font-style: normal;
    font-weight: 600;
    font-size: 18px;
    line-height: 25px;

    h1 {
      color: rgba(0, 0, 0, 0.87);
      font-size: 18px;
    }

    span {
      color: rgba(0, 0, 0, 0.6);
      font-size: 14px;
    }
  }

  button {
    background: #ffffff;
    border: 2px solid #1b458d;
    box-sizing: border-box;
    border-radius: 8px;
    padding: 10px;
    flex: 1;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    margin-top: 20px;
    margin-bottom: 20px;
    width: 100%;
    font-family: 'Nunito';
    font-style: normal;
    font-weight: 600;
    font-size: 16px;
    line-height: 22px;
    color: #1b458d;

    img {
      margin-right: 10px;
    }

    :hover {
      filter: brightness(0.9);
    }
  }

  #history {
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    width: 100%;
    font-family: Nunito;
    font-style: normal;
    font-weight: 600;
    font-size: 16px;
    line-height: 22px;
    margin: 20px;
    color: #1b458d;

    img {
      margin-right: 10px;
    }
  }

  #btn-download {
    display: flex;
    flex-direction: row;
    align-items: center;

    span {
      width: 80%;
    }
  }
`;

export const Step2 = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;

  > div {
    background: #ffffff;
    box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
    border-radius: 8px;
    margin: 30px;
    width: 50%;
    padding: 20px;
    font-family: Nunito;
    font-style: normal;
    font-weight: 600;
    line-height: 25px;

    h3 {
      font-size: 18px;
      color: rgba(0, 0, 0, 0.87);
      margin-bottom: 10px;
    }

    p {
      font-size: 14px;
      color: rgba(0, 0, 0, 0.6);
    }

    span {
      font-size: 14px;
      color: #e02424;
    }
  }
`;

export const Step3 = styled.div`
  #header {
    background-color: #fff;
    box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
    border-radius: 8px;
    margin: 30px;
    text-align: center;
    padding: 20px;
    font-family: Nunito;
    font-style: normal;
    font-weight: 600;

    line-height: 25px;

    h3 {
      font-size: 18px;
      color: rgba(0, 0, 0, 0.87);
    }

    span {
      font-size: 14px;
      color: rgba(0, 0, 0, 0.87);
    }
  }

  #content {
    background-color: #fff;
    box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
    border-radius: 8px;
    margin: 30px;
    padding: 20px;

    hr {
      margin-bottom: 30px;
      margin-top: 30px;
      border: 1px solid rgba(0, 0, 0, 0.12);
    }

    #dados {
      display: flex;
      flex-direction: row;
      align-items: center;
      justify-content: space-between;
      font-family: 'Nunito';
      font-style: normal;
      font-weight: 600;
      line-height: 25px;

      h3 {
        font-size: 18px;
        color: rgba(0, 0, 0, 0.87);
        margin-bottom: 10px;
      }

      p {
        font-size: 14px;
        color: #6b9ff8;
      }

      span {
        font-size: 14px;
        color: rgba(0, 0, 0, 0.6);
      }
    }
  }
`;
