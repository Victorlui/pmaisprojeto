import React from 'react';

// import { Container } from './styles';

import Layout from '../../Layout';

const Home: React.FC = () => {
  return (
    <Layout>
      <h1>Home</h1>
    </Layout>
  );
};

export default Home;
