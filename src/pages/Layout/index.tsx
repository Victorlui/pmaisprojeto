import { Header, Menu } from '../../components';
import { Container, Content } from './styles';

interface LayoutProps {
  children: any;
}

const Layout = ({ children }: LayoutProps) => {
  return (
    <Container>
      <Header />

      <Content>
        <Menu />
        {children}
      </Content>
    </Container>
  );
};

export default Layout;
