import { useState } from 'react';
import { Button } from 'pmaistemplates-v2';
import Layout from '../../../Layout';
import { HeaderForm, Form } from '../../../../components';
import { GroupWhite } from '../../../../assets/icons';
import { Container } from './styles';

import Cadastro from '../components/Cadastro';
import Responsavel from '../components/Responsavel';

const AddStudent = () => {
  const [active, setActive] = useState(0);

  function handleClick(id: number) {
    setActive(id);
  }

  return (
    <Layout>
      <Container>
        <HeaderForm
          active={active}
          title="Nome do aluno"
          subTitle="Modificado em: 13/03/2021"
          tabs
          titleActive1="Cadastro"
          titleActive2="Responsável"
          onTabChange={(id: number) => {
            handleClick(id);
          }}
          buttonTop
        >
          <Button
            width="180px"
            label="Impersonar"
            icon={GroupWhite}
            onClick={() => {
              console.log('teste');
            }}
          />
        </HeaderForm>

        <Form>{active === 0 ? <Cadastro /> : <Responsavel />}</Form>
      </Container>
    </Layout>
  );
};

export default AddStudent;
