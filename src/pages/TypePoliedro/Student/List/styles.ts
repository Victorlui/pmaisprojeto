import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  background-color: #fafbfc;
`;

export const Form = styled.div`
  padding: 20px;
`;
