import { useHistory } from 'react-router-dom';
import { Button, Row, Divider } from 'pmaistemplates-v2';
import { FormControl, Select, MenuItem, InputLabel } from '@material-ui/core';

import Layout from '../../../Layout';
import { HeaderForm, Table } from '../../../../components';
import { headCells } from '../../../../components/Table/header';
import { AddWhite } from '../../../../assets/icons';
import { Container, Form } from './styles';

const List = () => {
  const history = useHistory();
  return (
    <Layout>
      <Container>
        <HeaderForm
          active={1}
          title="Lista de Alunos"
          subTitle=""
          tabs={false}
          buttonTop
        >
          <Button
            label="Adicionar aluno"
            width="250px"
            icon={AddWhite}
            onClick={() => {
              history.push('/poliedro/addStudent');
            }}
          />
        </HeaderForm>

        <Form>
          <Row>
            <FormControl
              variant="outlined"
              style={{ background: '#fff', width: '100%', margin: 10 }}
            >
              <InputLabel id="demo-simple-select-outlined-label">
                Escola
              </InputLabel>
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                label="Escola"
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={10}>Ten</MenuItem>
                <MenuItem value={20}>Twenty</MenuItem>
                <MenuItem value={30}>Thirty</MenuItem>
              </Select>
            </FormControl>
            <FormControl
              variant="outlined"
              style={{ background: '#fff', width: '100%', margin: 10 }}
            >
              <InputLabel id="demo-simple-select-outlined-label">
                Curso
              </InputLabel>
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                label="Curso"
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={10}>Ten</MenuItem>
                <MenuItem value={20}>Twenty</MenuItem>
                <MenuItem value={30}>Thirty</MenuItem>
              </Select>
            </FormControl>
            <FormControl
              variant="outlined"
              style={{ background: '#fff', width: '100%', margin: 10 }}
            >
              <InputLabel id="demo-simple-select-outlined-label">
                Perfil
              </InputLabel>
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                label="Escola"
              >
                <MenuItem value="">
                  <em>Perfil</em>
                </MenuItem>
                <MenuItem value={10}>Ten</MenuItem>
                <MenuItem value={20}>Twenty</MenuItem>
                <MenuItem value={30}>Thirty</MenuItem>
              </Select>
            </FormControl>
            <FormControl
              variant="outlined"
              style={{ background: '#fff', width: '100%', margin: 10 }}
            >
              <InputLabel id="demo-simple-select-outlined-label">
                Status
              </InputLabel>
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                label="Status"
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={10}>Ten</MenuItem>
                <MenuItem value={20}>Twenty</MenuItem>
                <MenuItem value={30}>Thirty</MenuItem>
              </Select>
            </FormControl>
          </Row>
          <Divider />
          <Table
            data={[]}
            limit={5}
            onChangeLimit={value => {
              console.log(value);
            }}
            headCells={headCells}
          />
        </Form>
      </Container>
    </Layout>
  );
};

export default List;
