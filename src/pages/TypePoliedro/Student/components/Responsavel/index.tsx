import { useState } from 'react';
import { Title, Row, Column, Divider, Button } from 'pmaistemplates-v2';
import { Key, Delete, Save, Add } from '../../../../../assets/icons';
import { Input, Linha, TitleForm, Dialog } from '../../../../../components';

const Responsavel = () => {
  const [count, setCount] = useState(1);
  const [open, setOpen] = useState(false);

  function getRowResponsavel() {
    if (count < 5) {
      setCount(num => num + 1);
    }
  }

  return (
    <div>
      <TitleForm>
        <Title size="medium">Responsável</Title>
      </TitleForm>

      {[...Array(count)].map((value: undefined, index: number) => {
        let disa = null;
        if (index === 0) {
          disa = true;
        } else {
          disa = false;
        }

        return (
          <Column>
            <Linha>
              <Input variant="outlined" label="Nome" />
              <Input variant="outlined" label="E-mail" />
              <Button
                primary={false}
                round={false}
                icon={Key}
                onClick={() => console.log('cancelar')}
              />
              <div style={{ margin: 2 }} />
              <Button
                primary={false}
                round={false}
                icon={Delete}
                onClick={() => setOpen(true)}
              />
            </Linha>
            <Linha>
              <Input variant="outlined" label="Telefone" />
              <Input variant="outlined" label="Tipo" />
            </Linha>
            <Divider />
          </Column>
        );
      })}
      {count < 5 && (
        <TitleForm>
          <Button
            width="25%"
            label="Adicionar responsável"
            icon={Add}
            round={false}
            primary={false}
            onClick={() => {
              getRowResponsavel();
            }}
          />
        </TitleForm>
      )}

      <Row>
        <div />
        <Row width="250px">
          <Button
            primary={false}
            round
            label="Cancelar"
            onClick={() => console.log('cancelar')}
          />
          <Button
            primary
            round
            label="Salvar"
            icon={Save}
            onClick={() => console.log('salvar')}
          />
        </Row>
      </Row>

      <Dialog
        title="Excluir responsável"
        content="Tem certeza quer deseja excluir o responsável ?"
        open={open}
        handleClose={() => {
          setOpen(false);
        }}
        handleOk={() => {
          setCount(num => num - 1);
          setOpen(false);
        }}
      />
    </div>
  );
};

export default Responsavel;
