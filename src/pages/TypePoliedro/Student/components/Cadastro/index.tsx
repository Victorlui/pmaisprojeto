import React from 'react';
import {
  Title,
  Row,
  Column,
  UploadFiles,
  Button,
  Divider,
} from 'pmaistemplates-v2';
import { FormControl, Select, MenuItem, InputLabel } from '@material-ui/core';
import { Key, Save } from '../../../../../assets/icons';
import { headCellsStudent } from '../../../../../components/Table/header';
import { Input, Linha, Table, TitleForm } from '../../../../../components';

const Cadastro: React.FC = () => {
  return (
    <div>
      <TitleForm>
        <Title size="medium">Cadastro</Title>
      </TitleForm>

      <Row>
        <Row width="300px">
          <UploadFiles
            handle={() => {
              console.log('upload');
            }}
            title="Adicionar imagem"
            subtitle="Ou arraste a imagem aqui"
          />
        </Row>
        <Column>
          <Input
            label="Nome"
            variant="outlined"
            value="Lorem ipsum"
            style={{ width: '98%' }}
          />
          <Linha>
            <Input
              label="Matricula"
              variant="outlined"
              value="Lorem ipsum"
              style={{ width: '100%' }}
            />
            <Input label="E-mail P4ED" variant="outlined" value="Lorem ipsum" />
            <Button
              primary={false}
              round={false}
              icon={Key}
              onClick={() => console.log('add admin')}
            />
          </Linha>
        </Column>
      </Row>
      <Row>
        <Input label="E-mail" variant="outlined" value="Lorem ipsum" />
        <Input label="Telefone" variant="outlined" value="Lorem ipsum" />
        <Input label="Acessibilidade" variant="outlined" value="Lorem ipsum" />
      </Row>
      <Divider />

      <TitleForm>
        <Row width="270px">
          <strong style={{ fontSize: 24 }}>Histórico</strong>
          <FormControl
            variant="outlined"
            style={{ background: '#fff', width: 130 }}
          >
            <InputLabel id="demo-simple-select-outlined-label">
              Ano Letivo
            </InputLabel>
            <Select
              autoWidth
              labelId="demo-simple-select-outlined-label"
              id="demo-simple-select-outlined"
              label="Ano Letivo"
              style={{ height: 50 }}
            >
              <MenuItem value="">
                <em>Selecione</em>
              </MenuItem>
              <MenuItem value={5}>2021</MenuItem>
              <MenuItem value={10}>2020</MenuItem>
              <MenuItem value={20}>2019</MenuItem>
              <MenuItem value={30}>2018</MenuItem>
            </Select>
          </FormControl>
        </Row>
      </TitleForm>

      <Table
        data={[]}
        limit={5}
        onChangeLimit={value => {
          console.log(value);
        }}
        headCells={headCellsStudent}
      />
      <Divider />
      <Row>
        <div />
        <Row width="250px">
          <Button
            primary={false}
            round
            label="Cancelar"
            onClick={() => console.log('cancelar')}
          />
          <Button
            primary
            round
            label="Salvar"
            icon={Save}
            onClick={() => console.log('salvar')}
          />
        </Row>
      </Row>
    </div>
  );
};

export default Cadastro;
