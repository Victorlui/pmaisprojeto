import React, { useState } from 'react';
import { Button } from 'pmaistemplates-v2';
import Layout from '../../../Layout';
import { HeaderForm, Form } from '../../../../components';
import { Cloud, ClipBoard } from '../../../../assets/icons';
import Cadastro from '../components/Cadastro';
import Licencas from '../components/Licencas';

import { Container } from './styles';

const Create: React.FC = () => {
  const [active, setActive] = useState(0);

  function handleClick(id: number) {
    setActive(id);
  }

  return (
    <Layout>
      <Container>
        <HeaderForm
          active={active}
          title="Nome da escola"
          subTitle="Modificado em: 13/03/2021"
          tabs
          titleActive1="Cadastro"
          titleActive2="Licença"
          onTabChange={(id: number) => {
            handleClick(id);
          }}
          buttonTop
        >
          {active === 0 ? (
            <Button
              width="400px"
              label="Importar usuários escola teste"
              icon={Cloud}
              onClick={() => {
                console.log('teste');
              }}
            />
          ) : (
            <Button
              width="220px"
              label="Log de créditos"
              icon={ClipBoard}
              onClick={() => {
                console.log('teste');
              }}
            />
          )}
        </HeaderForm>
        {active === 0 ? (
          <Form>
            <Cadastro />
          </Form>
        ) : (
          <Licencas />
        )}
      </Container>
    </Layout>
  );
};

export default Create;
