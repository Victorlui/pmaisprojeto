import React, { useState } from 'react';
import {
  Title,
  Button,
  TagButton,
  Column,
  Select,
  Row,
  UploadFiles,
  Divider,
} from 'pmaistemplates-v2';
import { Add, Key, Delete, Save } from '../../../../../assets/icons';
import { TitleBox, RowSchool, ColumnSchool } from '../../Create/styles';
import {
  Input,
  Dialog,
  Modal,
  Linha,
  TitleForm,
} from '../../../../../components';

type Options = {
  id: number;
  title: string;
};

const initialOptions: Options[] = [
  {
    id: 1,
    title: 'Unidade 1',
  },
  {
    id: 2,
    title: 'Unidade 2',
  },
  {
    id: 3,
    title: 'Unidade 3',
  },
  {
    id: 4,
    title: 'Unidade 4',
  },
];

const initialPerfil: Options[] = [
  {
    id: 1,
    title: 'Perfil 1',
  },
  {
    id: 2,
    title: 'Perfil 2',
  },
  {
    id: 3,
    title: 'Perfil 3',
  },
  {
    id: 4,
    title: 'Perfil 4',
  },
];

const Cadastro: React.FC = () => {
  const [count, setCount] = useState(1);
  const [open, setOpen] = useState(false);
  const [openModal, setOpenModal] = useState(false);

  function getRowAdmin() {
    if (count < 5) {
      setCount(num => num + 1);
    }
  }

  return (
    <div>
      <TitleForm>
        <Title size="medium">Cadastro</Title>
      </TitleForm>

      <Linha>
        <Input
          label="Unidade Escolar"
          id="name"
          type="text"
          value="Name"
          variant="outlined"
        />
        <Input
          label="Nome da conta"
          id="nameofconta"
          type="text"
          value="Name"
          variant="outlined"
        />
      </Linha>

      <Linha>
        <Input
          label="ID Escola"
          id="idschool"
          type="text"
          value="123"
          variant="outlined"
        />

        <Input
          label="Padrão ano Letivo"
          id="anoLetivo"
          type="text"
          value="123"
          variant="outlined"
        />

        <Select
          width="100%"
          name="status"
          label="Status"
          options={initialPerfil}
          onSelect={(event: React.ChangeEvent<HTMLSelectElement>) => {
            console.log(event.target.value);
          }}
        />
      </Linha>

      <Linha>
        <Input
          label="Id Sales Force"
          id="idsales"
          type="text"
          value="BIRbKAAx"
          variant="outlined"
        />
        <Input
          label="Cidade"
          id="cidade"
          type="text"
          value="Cidade"
          variant="outlined"
        />
      </Linha>

      <Linha>
        <Input label="CNPJ" id="cnpj" type="text" value="" variant="outlined" />
        <Input
          label="Telefone"
          id="telefone"
          type="text"
          value="telefone"
          variant="outlined"
        />
      </Linha>

      <Linha>
        <Row width="30%">
          <UploadFiles
            handle={() => {
              console.log('upload');
            }}
            title="Adicionar imagem"
            subtitle="Ou arraste a imagem aqui"
          />
        </Row>
        <Input
          variant="outlined"
          id="descricao"
          label="Descrição"
          rows={7}
          multiline
        />
      </Linha>

      <Divider />

      <TitleForm>
        <RowSchool>
          <ColumnSchool>
            <Title size="medium">Unidade Escolar</Title>

            <TagButton
              label="CNPJ Escola"
              onClick={() => {
                console.log('add admin');
              }}
              icon={Add}
            />

            <Button
              label="Adicionar Escola"
              primary={false}
              round={false}
              width="200px"
              icon={Add}
              onClick={() => setOpenModal(true)}
            />
          </ColumnSchool>
          <ColumnSchool>
            <Title size="medium">Rede ID</Title>
            <TagButton
              label="CNPJ Escola"
              onClick={() => {
                console.log('add admin');
              }}
              icon={Add}
            />
          </ColumnSchool>
        </RowSchool>
      </TitleForm>
      <Divider />

      <TitleForm>
        <Title size="medium">Usuários administradores</Title>
      </TitleForm>

      {[...Array(count)].map((value: undefined, index: number) => {
        let disa = null;
        if (index === 0) {
          disa = true;
        } else {
          disa = false;
        }

        return (
          <Linha>
            <Input
              label="Nome"
              id="nameadmin"
              type="text"
              value="nome"
              variant="outlined"
            />
            <Input
              label="CPF"
              id="cpf"
              type="text"
              value="cpf"
              variant="outlined"
            />
            <Input
              label="E-mail"
              id="email"
              type="text"
              value="E-mail"
              variant="outlined"
            />
            <Input
              label="E-mail P4ED"
              id="emailP4ed"
              type="text"
              value="Email"
              variant="outlined"
            />

            <Button
              primary={false}
              round={false}
              icon={Key}
              onClick={() => console.log('add admin')}
            />
            <div style={{ margin: 5 }} />
            <Button
              primary={false}
              round={false}
              disabled={disa}
              icon={Delete}
              onClick={() => setOpen(true)}
            />
          </Linha>
        );
      })}
      {count < 5 && (
        <TitleForm>
          <Button
            label="Adicionar Adminstrador"
            primary={false}
            round={false}
            width="250px"
            icon={Add}
            onClick={() => {
              getRowAdmin();
            }}
          />
        </TitleForm>
      )}

      <Divider />

      <Column>
        <TitleForm>
          <Title size="medium">Consultor</Title>
        </TitleForm>
        <Linha>
          <Input
            label="Nome"
            id="nomeconsultor"
            type="text"
            value="Nome"
            variant="outlined"
          />
          <Input
            label="E-mail"
            id="emailconsultor"
            type="text"
            value="Email"
            variant="outlined"
          />
        </Linha>
      </Column>
      <Divider />
      <Row>
        <div />
        <Row width="250px">
          <Button
            label="Cancelar"
            primary={false}
            onClick={() => {
              console.log('save');
            }}
          />
          <div style={{ margin: '10px' }} />
          <Button
            icon={Save}
            label="Salvar"
            onClick={() => {
              console.log('save');
            }}
          />
        </Row>
      </Row>

      <Dialog
        title="Excluir administrador"
        content="Tem certeza quer deseja excluir o administrador ?"
        open={open}
        handleClose={() => {
          setOpen(false);
        }}
        handleOk={() => {
          setCount(num => num - 1);
          setOpen(false);
        }}
      />

      <Modal
        open={openModal}
        title="Unidade Escolar"
        handleClose={() => {
          setOpenModal(false);
        }}
      >
        <div style={{ width: '350px' }}>
          <h1>Unidade</h1>
        </div>
      </Modal>
    </div>
  );
};

export default Cadastro;
