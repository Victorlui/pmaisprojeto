import React from 'react';
import { Select, Title, Link } from 'pmaistemplates-v2';
import { useHistory } from 'react-router-dom';
import { Row, Container, Spacer, Content } from './styles';

type Options = {
  id: number;
  title: string;
};

const initialOptions: Options[] = [
  {
    id: 1,
    title: '06/2021',
  },
  {
    id: 2,
    title: '07/2021',
  },
  {
    id: 3,
    title: '08/2021',
  },
  {
    id: 4,
    title: '10/2021',
  },
];

const initialPerfil: Options[] = [
  {
    id: 1,
    title: 'Nome conta',
  },
  {
    id: 2,
    title: 'Nome conta',
  },
  {
    id: 3,
    title: 'Nome conta',
  },
  {
    id: 4,
    title: 'Nome conta',
  },
];

const Licencas: React.FC = () => {
  const history = useHistory();
  return (
    <Container>
      <Row>
        <Select
          width="250px"
          label="Ano Letivo"
          name="anoletivo"
          onSelect={(event: React.ChangeEvent<HTMLSelectElement>) => {
            console.log(event.target.value);
          }}
          options={initialOptions}
        />
        <Spacer />
        <Select
          width="250px"
          label="Conta"
          name="conta"
          onSelect={(event: React.ChangeEvent<HTMLSelectElement>) => {
            console.log(event.target.value);
          }}
          options={initialPerfil}
        />
      </Row>
      <Content>
        <Row>
          <Title size="medium">Educação Infantil:</Title>
          <Spacer />
          <Link
            href="##"
            onClick={() => {
              history.push(`./poliedro/creditBook`);
            }}
            decoration
          >
            G1 (30)
          </Link>
          <Spacer />
          <Link
            href="##"
            onClick={() => {
              history.push(`./poliedro/creditBook`);
            }}
          >
            G2 (30)
          </Link>
          <Spacer />
          <Link
            href="##"
            onClick={() => {
              history.push(`./poliedro/creditBook`);
            }}
          >
            G3 (30)
          </Link>
          <Spacer />
          <Link
            href="##"
            onClick={() => {
              history.push(`./poliedro/creditBook`);
            }}
          >
            G4 (30)
          </Link>
          <Spacer />
          <Link
            href="##"
            onClick={() => {
              history.push(`./poliedro/creditBook`);
            }}
          >
            G5 (30)
          </Link>
        </Row>
      </Content>
    </Container>
  );
};

export default Licencas;
