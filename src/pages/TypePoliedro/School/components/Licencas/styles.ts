import styled from 'styled-components';

export const Container = styled.div`
  margin-left: 40px;
  margin-right: 40px;
  margin-top: 20px;

  @media (min-width: 481px) and (max-width: 767px) {
    margin-left: 10px;
    margin-right: 10px;
  }

  @media (min-width: 320px) and (max-width: 480px) {
    margin-left: 10px;
    margin-right: 10px;
  }

  @media (min-width: 768px) and (max-width: 1024px) {
    margin-left: 10px;
    margin-right: 10px;
  }
`;

export const Row = styled.div`
  display: flex;
  align-items: center;
  flex-direction: row;
`;

export const Content = styled.div`
  box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
  background-color: #fff;
  border-radius: 8px;
  padding: 24px;
  height: 50vh;
`;

export const Spacer = styled.div`
  margin: 5px;
`;
