import React, { useEffect, useState, useCallback } from 'react';
import { Button, Divider, Row, Select } from 'pmaistemplates-v2';
import { useHistory } from 'react-router-dom';
import {
  OutlinedInput,
  InputAdornment,
  CircularProgress,
} from '@material-ui/core';
/* redux  */
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import * as SchoolActions from '../../../../store/modules/schools/actions';
import { StoreState } from '../../../../store/createStore';
import { SchoolProps } from '../../../../store/modules/schools/types';

import { HeaderForm, Table } from '../../../../components';
import { headCellsSchools } from '../../../../components/Table/header';
import { Container, Form, Spacer, LoadingBox } from './styles';
import { AddWhite, Search } from '../../../../assets/icons';
import Layout from '../../../Layout';

interface StateProps {
  schools: SchoolProps[];
  pages: number;
  totalItems: number;
  loading: boolean;
}

interface DispatchProps {
  getSchoolRequest(limit: number, unidadeId?: string, status?: number): void;
}

type Props = StateProps & DispatchProps;

type Options = {
  id: number;
  title: string;
  status: number;
};

const initialPerfil: Options[] = [
  {
    id: 1,
    title: 'Segmento 1',
    status: 1,
  },
  {
    id: 2,
    title: 'Segmento 2',
    status: 1,
  },
  {
    id: 3,
    title: 'Segmento 3',
    status: 1,
  },
  {
    id: 4,
    title: 'Segmento 4',
    status: 1,
  },
];

const initialStatus: Options[] = [
  {
    id: 1,
    title: 'Ativo',
    status: 1,
  },
  {
    id: 2,
    title: 'Inativo',
    status: 0,
  },
];

const schoolsList = [
  {
    id: 1,
    nomeConta: 'Poliedro São José dos Campos',
    cidade: 'São Paulo',
    segment: 'EI / EFAI / EFAF / EM / PV',
    ativo: '0',
  },
  {
    id: 2,
    nomeConta: 'Poliedro São José do Rio Preto',
    cidade: 'São Paulo',
    segment: 'EI / EFAI / EFAF / EM / PV',
    ativo: '1',
  },
];

const List = (props: Props) => {
  const history = useHistory();
  const { getSchoolRequest, schools, loading, pages, totalItems } = props;

  const [limit, setLimit] = useState(5);
  const [search, setSearch] = useState('');
  const [statusSchool, setStatusSchool] = useState(4);

  const handleCreate = () => {
    history.push('/poliedro/createSchool');
  };

  useEffect(() => {
    getSchoolRequest(5, '', undefined);
  }, []);

  function changeLimitSchool(id: number) {
    setLimit(id);
    if (statusSchool === 4) {
      getSchoolRequest(id, search, undefined);
    }
    getSchoolRequest(id, search, statusSchool);
  }

  function selectActive(status: number) {
    getSchoolRequest(limit, search, status);
  }

  function searchText(unidadeId: string) {
    getSchoolRequest(limit, unidadeId, statusSchool);
  }

  return (
    <Layout>
      <Container>
        <HeaderForm
          active={1}
          title="Lista de Escolas"
          subTitle=""
          tabs={false}
          buttonTop
        >
          <Button
            label="Adicionar uma escola"
            width="250px"
            icon={AddWhite}
            onClick={() => {
              handleCreate();
            }}
          />
        </HeaderForm>
        <Form>
          <Row>
            <OutlinedInput
              style={{ width: '100%', margin: '10px', background: 'white' }}
              value={search}
              placeholder="Pesquise pela Unidade Escolar ou ID Escola"
              onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                setSearch(event.target.value);
                searchText(event.target.value);
              }}
              endAdornment={
                <InputAdornment position="end">
                  <img alt="search" src={Search} />
                </InputAdornment>
              }
            />

            <Select
              width="50%"
              name="segmento"
              label="Segmento"
              options={initialPerfil}
              onSelect={(event: React.ChangeEvent<HTMLSelectElement>) => {
                searchText(event.target.value);
              }}
            />
            <Spacer />
            <Select
              width="50%"
              name="status"
              label="Status"
              options={initialStatus}
              onSelect={(event: React.ChangeEvent<HTMLSelectElement>) => {
                if (event.target.value === '1') {
                  selectActive(1);
                  setStatusSchool(1);
                } else {
                  selectActive(0);
                  setStatusSchool(0);
                }
              }}
            />
          </Row>
          <Divider />
          {loading ? (
            <LoadingBox>
              <CircularProgress color="primary" />
            </LoadingBox>
          ) : (
            <Table
              data={schoolsList}
              limit={limit}
              pagesNavigation={pages}
              totalItems={totalItems}
              onChangeLimit={value => {
                changeLimitSchool(value);
              }}
              headCells={headCellsSchools}
            />
          )}
        </Form>
      </Container>
    </Layout>
  );
};

const mapStateToProps = (state: StoreState) => ({
  schools: state.schools.schools,
  pages: state.schools.pages,
  totalItems: state.schools.totalItems,
  loading: state.schools.loading,
});

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(SchoolActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(List);
