import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  background-color: #fafbfc;
`;

export const Form = styled.div`
  padding: 20px;
`;

export const Spacer = styled.div`
  margin: 5px;
`;

export const LoadingBox = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 37vh;
`;
