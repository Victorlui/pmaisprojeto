import styled from 'styled-components';

export const Container = styled.div`
  background-color: #fafbfc;
  padding: 20px;

  img {
    cursor: pointer;
  }
`;
