import React, { useState } from 'react';
import { Column, Button, Divider, Row } from 'pmaistemplates-v2';
import Layout from '../../../Layout';
import { HeaderForm, Pedding, Completed, Linha } from '../../../../components';
import { Save } from '../../../../assets/icons';
import Arrow from '../../../../assets/arrow_circle.png';
import { Container } from './styles';

type Options = {
  id: number;
  title: string;
  status: string;
};

const initialOptions: Options[] = [
  {
    id: 1,
    title: 'Livro',
    status: 'Pending',
  },
  {
    id: 2,
    title: 'Livro',
    status: 'Pending',
  },
  {
    id: 3,
    title: 'Livro',
    status: 'Pending',
  },
  {
    id: 4,
    title: 'Livro',
    status: 'Pending',
  },
  {
    id: 5,
    title: 'Livro',
    status: 'Pending',
  },
  {
    id: 6,
    title: 'Livro',
    status: 'Pending',
  },
  {
    id: 7,
    title: 'Livro',
    status: 'Pending',
  },
  {
    id: 8,
    title: 'Livro',
    status: 'Pending',
  },
  {
    id: 9,
    title: 'Livro',
    status: 'Pending',
  },
  {
    id: 10,
    title: 'Livro',
    status: 'Pending',
  },
];

const NewMaterial: React.FC = () => {
  const [items, setItems] = useState(initialOptions);

  const updateStatus = (id: number, newStatus: string) => {
    let allItems = items;
    allItems = allItems.map(item => {
      if (item.id === id) {
        console.log('in here');
        item.status = newStatus;
      }
      return item;
    });
    setItems(allItems);
  };
  return (
    <Layout>
      <Column>
        <HeaderForm active={1} title="Livro extras" subTitle="EFAF - 6º ANO" />
        <Container>
          <Linha>
            <Column>
              <h3>Livro</h3>
              <Pedding
                items={items}
                setItems={setItems}
                updateStatus={updateStatus}
              />
            </Column>
            <img src={Arrow} alt="arrow" />
            <Column>
              <h3>Configuração</h3>
              <Completed
                items={items}
                setItems={setItems}
                updateStatus={updateStatus}
              />
            </Column>
          </Linha>

          <Divider />

          <Row>
            <div />
            <Row width="250px">
              <Button
                label="Cancelar"
                primary={false}
                onClick={() => {
                  console.log('save');
                }}
              />
              <div style={{ margin: '10px' }} />
              <Button
                icon={Save}
                label="Salvar"
                onClick={() => {
                  console.log('save');
                }}
              />
            </Row>
          </Row>
        </Container>
      </Column>
    </Layout>
  );
};

export default NewMaterial;
