import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  background-color: #fafbfc;

  h3 {
    font-size: 24px;
    line-height: 33px;
    margin-left: 20px;
    color: rgba(0, 0, 0, 0.87);
  }
`;

export const CardCredit = styled.div`
  background-color: #fff;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 10px;
  margin: 20px;
  background: #ffffff;
  /* Box-shadow-1 */

  box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.1);
  border-radius: 8px;

  hr {
    width: 1px;
    height: 200px;
    margin: 0px;
    /* Greys/grey_extra_light */

    border: 1px solid rgba(0, 0, 0, 0.12);
  }

  @media (min-width: 481px) and (max-width: 767px) {
    flex-direction: column;
    hr {
      height: 1px;
      width: 100%;
    }
  }

  @media (min-width: 320px) and (max-width: 480px) {
    flex-direction: column;
    hr {
      height: 1px;
      width: 100%;
    }
  }

  @media (min-width: 768px) and (max-width: 1024px) {
    flex-direction: column;
    hr {
      height: 1px;
      width: 100%;
    }
  }
`;

export const Form = styled.div`
  display: flex;
  flex-direction: column;
  padding: 10px;
  font-family: Nunito;
  /* identical to box height */

  /* Greys/grey_medium */
`;

export const CreditForm = styled.div`
  margin-bottom: 10px;
  margin-left: 10px;
  div {
    display: flex;
    align-items: center;
  }

  strong {
    margin: 0;
    font-size: 14px;
    line-height: 19px;
    /* identical to box height */

    /* Greys/grey_medium */

    color: rgba(0, 0, 0, 0.6);
  }
`;

export const CreditTemp = styled.div`
  width: 50%;
  @media (min-width: 481px) and (max-width: 767px) {
    width: 100%;
  }

  @media (min-width: 320px) and (max-width: 480px) {
    width: 100%;
  }

  @media (min-width: 768px) and (max-width: 1024px) {
    width: 100%;
  }
`;

export const ButtonIncre = styled.button`
  border: 0;
  background: #1b458d;
  border-radius: 6px;
  width: 40px;
  height: 40px;
  padding: 8px 12px;
  display: flex;
  align-items: center;
  justify-content: center;
`;
