import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Row, Divider, Title, Button } from 'pmaistemplates-v2';
import Layout from '../../../Layout';
import {
  Livro,
  BookStar,
  TicketsExtra,
  Sub,
  AddWhite,
  Tickets,
} from '../../../../assets/icons';
import Img from '../../../../assets/char.png';
import { CardList, HeaderForm, Input, Linha } from '../../../../components';
import {
  Container,
  Form,
  CreditForm,
  CardCredit,
  CreditTemp,
  ButtonIncre,
} from './styles';

const CreditBook = () => {
  const history = useHistory();
  const [count, setCount] = useState(0);
  return (
    <Layout>
      <Container>
        <HeaderForm
          active={1}
          title="EFAF - 6º ANO"
          subTitle="Modificado em 13/03/2021"
          tabs
          titleActive1="EFAF - 6º ANO(expe)"
          titleActive2="EFAF - 6º ANO(integral)"
          buttonTop
        >
          <Button
            label="Novo Curso"
            width="150px"
            icon={AddWhite}
            onClick={() => {
              console.log('novo curso');
            }}
          />
        </HeaderForm>
        <Form>
          <Row>
            <Input variant="outlined" label="Nome do curso" />

            <Input variant="outlined" label="Curso padrão" disabled />
          </Row>

          <Divider />
          <div style={{ margin: '10px' }} />
          <h3>Créditos</h3>
          <CardCredit>
            <CreditForm>
              <div>
                <img src={Tickets} alt="tickets" style={{ margin: '10px' }} />
                <Title size="medium">Créditos</Title>
              </div>
              <img src={Img} alt="img" />
            </CreditForm>
            <hr />
            <CreditTemp>
              <CreditForm>
                <div>
                  <img
                    src={TicketsExtra}
                    alt="tickets"
                    style={{ margin: '10px' }}
                  />
                  <Title size="medium">Créditos Temporários</Title>
                </div>
                <strong>Adicione ou remova créditos temporários</strong>
              </CreditForm>

              <Linha>
                <ButtonIncre
                  type="button"
                  onClick={() => {
                    if (count > 0) {
                      setCount(num => num - 1);
                    }
                  }}
                >
                  <img src={Sub} alt="sub" />
                </ButtonIncre>
                <Input
                  variant="outlined"
                  label="Adicionar/Remover"
                  value={count}
                  style={{ padding: 0 }}
                />
                <ButtonIncre
                  type="button"
                  onClick={() => {
                    setCount(num => num + 1);
                  }}
                >
                  <img src={AddWhite} alt="add" />
                </ButtonIncre>
                <Input variant="outlined" label="Validade" />
              </Linha>
            </CreditTemp>
          </CardCredit>

          <h3>Livros</h3>
          <Row>
            <CardList
              label="Integração"
              iconHeader={Livro}
              showFooter={false}
              iconList
              showCheck
              onClickFooter={() => {
                console.log('botão footer');
              }}
              optionsList={false}
            />
            <CardList
              label="Livro extra"
              iconHeader={BookStar}
              iconList={false}
              showCheck={false}
              showFooter
              onClickFooter={() => {
                history.push('/poliedro/newMaterial');
              }}
              optionsList
            />
          </Row>
        </Form>
      </Container>
    </Layout>
  );
};

export default CreditBook;
