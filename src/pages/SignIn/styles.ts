import styled from 'styled-components';
import Bg from '../../assets/bg.png';

export const Container = styled.div`
  background: url(${Bg}) no-repeat;
  background-size: cover;
  flex: 1;
  height: 100vh;
  width: 100vw;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Form = styled.form`
  padding: 10px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: column;
`;
