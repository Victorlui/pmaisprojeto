import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { Button, Card, Img, Link, Select } from 'pmaistemplates-v2';
import { Input } from '../../components';
import Logo from '../../assets/logo.png';

import { settype } from '../../store/modules/auth/actions';

import { Container, Form } from './styles';

type Options = {
  id: number;
  title: string;
};

type FormData = {
  email: string;
  password: string;
  unidade: number;
  perfil: number;
};

const initialOptions: Options[] = [
  {
    id: 1,
    title: 'Unidade 1',
  },
  {
    id: 2,
    title: 'Unidade 2',
  },
  {
    id: 3,
    title: 'Unidade 3',
  },
  {
    id: 4,
    title: 'Unidade 4',
  },
];

const initialPerfil: Options[] = [
  {
    id: 1,
    title: 'Poliedro',
  },
  {
    id: 2,
    title: 'Escola',
  },
];

const SignIn = () => {
  const dispacth = useDispatch();
  const history = useHistory();
  const [options] = useState(initialOptions);
  const [perfil] = useState(initialPerfil);

  const [Idunidade, setIdUnidade] = useState('');
  const [Idperfil, setIdPerfil] = useState('');

  const [acesso1, setAcesso1] = useState(true);
  const [acesso2, setAcesso2] = useState(false);
  const [acesso3, setAcesso3] = useState(false);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FormData>();

  const onSubmit = handleSubmit(data => {
    if (Idperfil === 'Selecione') {
      alert('Selecione um perfil');
    } else if (Idperfil === '') {
      alert('Selecione um perfil');
    } else if (Idperfil === '1') {
      dispacth(settype(Idperfil));
      history.push('/poliedro/listSchool');
    } else if (Idperfil === '2') {
      dispacth(settype(Idperfil));
      history.push('/school/listClasses');
    }
  });

  return (
    <Container>
      <Card>
        <Form onSubmit={onSubmit}>
          <Img src={Logo} />
          {acesso1 && (
            <>
              <Input
                id="email"
                key="email"
                label="E-mail"
                variant="outlined"
                {...register('email')}
              />

              <Input
                id="password"
                key="password"
                label="Senha"
                variant="outlined"
                type="password"
                {...register('password')}
              />

              <Button
                label="Login"
                width="100%"
                primary
                onClick={() => {
                  setAcesso1(false);
                  setAcesso2(true);
                }}
              />

              <Link href="www.google.com.br" style={{ margin: '10px' }}>
                Esquecí a senha
              </Link>
            </>
          )}

          {acesso2 && (
            <>
              <Select
                width="100%"
                name="unidade"
                label="Unidade"
                options={options}
                onSelect={(event: React.ChangeEvent<HTMLSelectElement>) => {
                  setIdUnidade(event.target.value);
                }}
              />

              <Button
                label="Acessar"
                primary
                width="100%"
                onClick={() => {
                  setAcesso2(false);
                  setAcesso3(true);
                }}
              />
            </>
          )}

          {acesso3 && (
            <>
              <Select
                width="100%"
                name="perfil"
                label="Perfil"
                options={perfil}
                onSelect={(event: React.ChangeEvent<HTMLSelectElement>) => {
                  setIdPerfil(event.target.value);
                }}
              />

              <Button
                label="Acessar"
                width="100%"
                primary
                onClick={() => {
                  console.log('entrou');
                }}
              />
            </>
          )}
        </Form>
      </Card>
    </Container>
  );
};

export default SignIn;
