import Home from './home.png';
import Add from './add.png';
import Cloud from './cloud.png';
import Delete from './delete.png';
import Key from './key.png';
import Save from './save.png';
import ClipBoard from './clipboard.png';
import Close from './close.png';
import Had from './had.png';
import Livro from './livro.png';
import Livro2 from './livro2.png';
import Group2 from './group2.png';
import Group from './group.png';
import GroupWhite from './group_white.png';
import AddWhite from './add_white.png';
import BookStar from './book_star.png';
import Tickets from './ticket.png';
import Sub from './sub.png';
import TicketsExtra from './ticket_extra.png';
import Search from './search.png';
import IdIcon from './id.png';
import Settings from './settings.png';
import ArrowDown from './arrow_down.png';
import Folder from './folder.png';
import Edit from './edit.png';
import Check from './check.png';
import Calendar from './calendar.png';
import DeleteGray from './delete_gray.png';
import Info from './info.png';
import Profile from './profile.png';
import CloseBlue from './close_blue.png';
import More from './more.png';
import Options from './options.png';
import Copy from './copy.png';
import Download from './download.png';
import History from './history.png';
import Send from './send.png';

export {
  Home,
  Add,
  Cloud,
  Delete,
  Key,
  Save,
  ClipBoard,
  Close,
  Had,
  Livro,
  Livro2,
  Group2,
  Group,
  AddWhite,
  BookStar,
  Tickets,
  Sub,
  TicketsExtra,
  Search,
  IdIcon,
  Settings,
  ArrowDown,
  Folder,
  Edit,
  GroupWhite,
  Check,
  Calendar,
  DeleteGray,
  Info,
  Profile,
  CloseBlue,
  More,
  Options,
  Copy,
  Download,
  History,
  Send,
};
