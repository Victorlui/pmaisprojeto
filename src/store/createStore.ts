import {
  createStore,
  Reducer,
  combineReducers,
  applyMiddleware,
  Middleware,
  compose,
  StoreEnhancer,
} from 'redux';
import type { AuthAction, AuthState } from './modules/auth/types';
import type { SchoolAction, SchoolState } from './modules/schools/types';
import type { PageAction, PageState } from './modules/page/types';

export interface StoreState {
  auth: AuthState;
  schools: SchoolState;
  page: PageState;
}

export type StoreAction = AuthAction;
export type StoreSchoolAction = SchoolAction;
export type StorePageAction = PageAction;

export default (
  reducers: Reducer<
    [StoreState, StoreAction, StoreSchoolAction, StorePageAction]
  >,
  middlewares: Middleware[],
) => {
  const enhancer: StoreEnhancer =
    process.env.NODE_ENV === 'development'
      ? compose(console.tron.createEnhancer(), applyMiddleware(...middlewares))
      : applyMiddleware(...middlewares);

  return createStore(reducers, enhancer);
};
