import { all } from 'redux-saga/effects';

import auth from './auth/sagas';
import getSchools from './schools/sagas';

export default function* rootSaga(): Generator<any> {
  return yield all([auth, getSchools]);
}
