import { action } from 'typesafe-actions';

export function getSchoolRequest(
  limit: number,
  unidadeId?: string,
  status?: number,
) {
  return action('@schools/GET_SCHOOL_REQUEST', { limit, unidadeId, status });
}

export function getSchoolSuccess(
  schools: any,
  pages: number,
  totalItems: number,
) {
  return action('@schools/GET_SCHOOL_SUCCESS', { schools, pages, totalItems });
}

export function getSchoolFailure() {
  return action('@schools/GET_SCHOOL_FAILURE');
}

/* Cadastro */

export function setSchoolRequest() {
  return action('@schools/POST_SCHOOL_REQUEST');
}

export function setSchoolSuccess() {
  return action('@schools/POST_SCHOOL_SUCCESS');
}

export function setSchoolFailure() {
  return action('@schools/POST_SCHOOL_FAILURE');
}
