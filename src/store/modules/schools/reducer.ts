import produce, { Draft } from 'immer';

import type { SchoolAction, SchoolState } from './types';

const INITIAL_STATE: SchoolState = {
  schools: [],
  loading: false,
  pages: 0,
  totalItems: 0,
};

export default function schools(state = INITIAL_STATE, action: SchoolAction) {
  return produce(state, (draft: Draft<SchoolState>) => {
    switch (action.type) {
      case '@schools/GET_SCHOOL_REQUEST': {
        draft.loading = true;
        break;
      }
      case '@schools/GET_SCHOOL_SUCCESS': {
        draft.schools = action.payload.schools;
        draft.pages = action.payload.pages;
        draft.totalItems = action.payload.totalItems;
        draft.loading = false;
        break;
      }
      case '@schools/GET_SCHOOL_FAILURE': {
        draft.loading = false;
        break;
      }
      default:
    }
  });
}
