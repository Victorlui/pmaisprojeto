import type { ActionType } from 'typesafe-actions';

import type * as actions from './actions';

export type SchoolAction = ActionType<typeof actions>;

export interface SchoolProps {
  id: number;
  nomeConta: string;
  cidade: string;
  ativo: string;
  segment: string;
}

export interface ClassesProps {
  id: number;
  name: string;
  user: string;
  matricula: string;
  status: string;
}

export interface ClassesListProps {
  id: number;
  nameTurma: string;
  segmento: string;
  serie: string;
  curso: string;
  licencas: string;
}

export interface UsersListProps {
  id: number;
  name: string;
  user: string;
  perfil: string;
  status: string;
}

export interface HistoricListProps {
  id: number;
  unidade: string;
  turma: string;
  curso: string;
  disciplina: string;
  status: string;
}

export interface ImportListProps {
  id: number;
  name: string;
  data: string;
  carregado: string;
}

export interface SchoolState {
  readonly schools: SchoolProps[];
  readonly loading: boolean;
  readonly pages: number;
  readonly totalItems: number;
}
