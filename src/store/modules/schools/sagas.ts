import { takeLatest, call, put, all } from 'redux-saga/effects';

import type { ActionType } from 'typesafe-actions';

import { api } from '../../../services/api';

import {
  getSchoolRequest,
  getSchoolSuccess,
  getSchoolFailure,
} from './actions';

export function* getSchools({
  payload,
}: ActionType<typeof getSchoolRequest>): any {
  try {
    if (payload.status === 0 || payload.status === 1) {
      const response = yield call(
        api.get,
        `http://localhost:5050/api/scholl/all?take=${payload.limit}&skip=0&ativo=${payload.status}&keyword=${payload.unidadeId}&page=1`,
      );

      yield put(
        getSchoolSuccess(
          response.data.data,
          response.data.pagination.pages,
          response.data.pagination.totalItems,
        ),
      );
    } else {
      const response = yield call(
        api.get,
        `http://localhost:5050/api/scholl/all?take=${payload.limit}&skip=0&keyword=${payload.unidadeId}&page=1`,
      );

      yield put(
        getSchoolSuccess(
          response.data.data,
          response.data.pagination.pages,
          response.data.pagination.totalItems,
        ),
      );
      console.log(response);
    }
  } catch (error) {
    yield put(getSchoolFailure());
  }
}

export default all([takeLatest('@schools/GET_SCHOOL_REQUEST', getSchools)]);
