import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import type { Reducer } from 'redux';

export default (reducers: Reducer) => {
  const persistedReducer = persistReducer(
    {
      key: 'pmais',
      storage,
      whitelist: ['auth', 'page'],
    },
    reducers,
  );
  return persistedReducer;
};
