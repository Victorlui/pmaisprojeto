import { action } from 'typesafe-actions';

export function pageRequest(page: number) {
  return action('@navigation/PAGE_REQUEST', { page });
}
