import produce, { Draft } from 'immer';

import type { PageAction, PageState } from './types';

const INITIAL_STATE: PageState = {
  page: 1,
};

export default function navigation(state = INITIAL_STATE, action: PageAction) {
  return produce(state, (draft: Draft<PageState>) => {
    switch (action.type) {
      case '@navigation/PAGE_REQUEST': {
        draft.page = action.payload.page;
        break;
      }
      default:
    }
  });
}
