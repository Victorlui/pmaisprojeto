import type { ActionType } from 'typesafe-actions';

import type * as actions from './actions';

export type PageAction = ActionType<typeof actions>;

export interface PageState {
  readonly page: number;
}
