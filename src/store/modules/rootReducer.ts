import { combineReducers } from 'redux';
import type { StoreState } from '../createStore';

import auth from './auth/reducer';
import schools from './schools/reducer';
import page from './page/reducer';

export default combineReducers<StoreState>({
  auth,
  schools,
  page,
});
