import { takeLatest, call, put, all } from 'redux-saga/effects';
import axios from 'axios';

import type { ActionType } from 'typesafe-actions';

import { api } from '../../../services/api';

import {
  signInRequest,
  signInSuccess,
  setUserId,
  signInFailure,
  setSigned,
  cancelLoading,
  emailError,
  passwordError,
} from './actions';

export function* signIn({ payload }: ActionType<typeof signInRequest>): any {
  try {
    const responseEmail = yield call(
      api.get,
      `signup/verify-non-existent-email/${payload.email}`,
    );

    if (responseEmail.status === 200) {
      try {
        const data = {
          username: payload.email,
          password: payload.password,
        };

        const response = yield call(api.post, 'oauth/token', data);

        if (response.status === 200) {
          const { access_token: token, userId } = response.data;

          axios.defaults.headers.Authorization = `Bearer ${token}`;

          yield put(setUserId(token, userId));
          yield put(signInSuccess(token));
          yield put(setSigned());
        }
      } catch (error) {
        yield put(cancelLoading());
        if (error.response) {
          switch (error.response.status) {
            case 500:
              yield put(signInFailure());
              break;
            case 409:
              yield put(signInFailure());
              yield put(emailError());
              break;
            case 404:
              yield put(signInFailure());
              break;
            case 401:
              yield put(signInFailure());
              break;
            case 400:
              yield put(passwordError());
              yield put(signInFailure());
              break;
            default:
              yield put(signInFailure());
              break;
          }
        }
      }
    }
  } catch (error) {
    switch (error.response.status) {
      case 500:
        yield put(signInFailure());
        break;
      case 404:
        yield put(signInFailure());
        break;
      default:
        yield put(signInFailure());
        break;
    }
  }
}

export default all([takeLatest('@auth/SIGN_IN_REQUEST', signIn)]);
