import produce, { Draft } from 'immer';

import type { AuthAction, AuthState } from './types';

const INITIAL_STATE: AuthState = {
  token: '',
  signed: false,
  loading: false,
  emailError: false,
  passwordError: false,
  userId: '',
  type: 1,
};

export default function auth(state = INITIAL_STATE, action: AuthAction) {
  return produce(state, (draft: Draft<AuthState>) => {
    switch (action.type) {
      case '@auth/SIGN_IN_REQUEST': {
        draft.loading = true;
        break;
      }
      case '@auth/SIGN_IN_SUCCESS': {
        draft.token = action.payload.token;
        draft.loading = false;
        break;
      }
      case '@auth/SIGN_IN_FAILURE': {
        draft.loading = false;
        break;
      }
      case '@auth/SET_USERID': {
        draft.userId = action.payload.userId;
        draft.token = action.payload.token;
        break;
      }
      case '@auth/CANCEL_LOADING': {
        draft.loading = false;
        break;
      }
      case '@auth/EMAIL_ERROR': {
        draft.emailError = true;
        break;
      }
      case '@auth/CLEAR_EMAIL_ERROR': {
        draft.emailError = false;
        break;
      }
      case '@auth/PASSWORD_ERROR': {
        draft.passwordError = true;
        break;
      }
      case '@auth/CLEAR_PASSWORD_ERROR': {
        draft.passwordError = false;
        break;
      }
      case '@auth/SET_SIGNED': {
        draft.signed = true;
        break;
      }
      case '@auth/SET_TYPE': {
        draft.type = action.payload.type;
        break;
      }
      case '@auth/SET_SIGN_OUT': {
        draft.signed = false;
        draft.emailError = false;
        draft.passwordError = false;
        draft.loading = false;
        draft.userId = '';
        draft.token = '';
        break;
      }
      default:
    }
  });
}
