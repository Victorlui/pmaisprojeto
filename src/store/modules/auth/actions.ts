import { action } from 'typesafe-actions';

export function signInRequest(email: string, password: string) {
  return action('@auth/SIGN_IN_REQUEST', { email, password });
}

export function signInSuccess(token: string) {
  return action('@auth/SIGN_IN_SUCCESS', { token });
}

export function signInFailure() {
  return action('@auth/SIGN_IN_FAILURE');
}

export function setUserId(token: string, userId: string) {
  return action('@auth/SET_USERID', { token, userId });
}

export function cancelLoading() {
  return action('@auth/CANCEL_LOADING');
}

export function emailError() {
  return action('@auth/EMAIL_ERROR');
}

export function clearEmailError() {
  return action('@auth/CLEAR_EMAIL_ERROR');
}

export function passwordError() {
  return action('@auth/PASSWORD_ERROR');
}

export function clearPasswordError() {
  return action('@auth/CLEAR_PASSWORD_ERROR');
}

export function setSigned() {
  return action('@auth/SET_SIGNED');
}

export function setSignOut() {
  return action('@auth/SET_SIGN_OUT');
}

export function settype(type: any) {
  return action('@auth/SET_TYPE', { type });
}
