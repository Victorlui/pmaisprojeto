import { setup } from 'axios-cache-adapter';

export const api = setup({
  baseURL: 'http://localhost:5050/api/',
  cache: {
    maxAge: 15 * 60 * 1000,
  },
});
