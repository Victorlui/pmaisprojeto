import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  * {
   margin:0;
   padding: 0;
   outline: 0;
   box-sizing: border-box;
  }

  html, body {
    height: 100vh;
  
  }

  *:focus {
    outline: 0;
  }

  body {
    -webkit-font-smoothing: antialiased;
  }

  body, input, button {
    font: 16px 'Nunito', sans-serif;
  }

  a {
    text-decoration:none;
  }

  ul {
    list-style: none;
  }

  button {
    cursor: pointer;
  }
`;
