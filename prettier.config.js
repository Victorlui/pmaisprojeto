module.exports = {
  singleQuote: true,
  trailingComma: 'all',
	arrowParens: 'avoid',
  "react/jsx-props-no-spreading": [, {
    "html": "ignore" | "enforce",
    "custom": "ignore" | "enforce",
    "explicitSpread": "ignore" | "enforce",
    "exceptions": []
}]
}
